import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import CurrentUser from '../../services/CurrentUser';
import roleTypes from '../../config/UserRoleTypes'

export default function (ComposedComponent) {
  class Authentication extends Component {
    componentWillMount() {
      if (this.props.authorized) {
        this._dashBoardRouter()
      }
    }

    componentWillUpdate(nextProps) {
      if (nextProps.authorized) {
        this._dashBoardRouter()
      }
    }

    _dashBoardRouter() {
      let {role} = new CurrentUser()
        switch(role) {
          case roleTypes.ADMIN:
            this.props.history.push('/home/admin/')
          break;
          case roleTypes.MANAGER:
            this.props.history.push('/home/manager/')
          break;
          case roleTypes.EMPLOYEE:
            this.props.history.push('/home/employee/')
          break;
          default:
            this.props.history.push('/')
          break;
        }
    }

    PropTypes = {
      router: PropTypes.object,
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  function mapStateToProps(state) {
    return { authorized: state.auth.authorized };
  }

  return connect(mapStateToProps)(Authentication);
}