import React, { Component } from 'react'
import { withRouter } from "react-router-dom";
import { Icon } from 'antd';
import { Link } from 'react-router-dom'
import routes from '../../config/Routes'
import CurrentUser from '../../services/CurrentUser'
import './nav.css'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { logout } from '../../store/actions/authAction'

class Navigation extends Component {

	state = {
		menuLinks: [],
		userName: ''
	}

	componentWillMount() {
		const user = new CurrentUser()
		const userName = JSON.parse(localStorage.getItem('user')).user.nameusersec
		const menuLinks = routes.filter((route) => {
				if (route.role === user.role && route.isMenuList)
						return route
		})
		this.setState({menuLinks, userName})
	}

	handleClick = e => {
		if (e.key === "0") {
			this.props.logout()
		} else {
			this.props.history.push(e.key)
		}
	}

	render () {
			const isOpen = this.props.isOpen
			const { userName } = this.state

			const menuItem = this.state.menuLinks.map((item, i) => {
				return <li onClick={() => {this.props.close()}} key={i}><Link to={item.path}>{item.name}</Link></li>
			})
			return isOpen ? 
			(<div  className="menu--wrapper">
				<div className="menu--top">
					<span className="dot">
						<Icon style={{ fontSize: '24px', color: '#348BCD' }} type="user" />
					</span>
					<div>
						<span className="span--welcome"> Welcome, </span> 
						<span className="span--name"> {userName} </span>
					</div>
				</div>
				<ul>
					{menuItem}
				</ul>

				<div className="menu--bottom">
					<span className="dot">
						<Icon style={{ fontSize: '24px', color: '#348BCD' }} type="logout" />
					</span>
					<a onClick={() => this.props.logout() }> Logout </a>
				</div>

			</div>
			):('')
	}
}

Navigation.prototypes = {
	logout: PropTypes.func.isRequired
}

export default connect(null, { logout })(withRouter(Navigation))