import React from 'react'
import CurrentUser from '../../services/CurrentUser'
import Page404 from '../Page404'
export default function Authorization (allowedRoles) {
    return function (WrappedComponent) {
        return function (props) {
            const { role } = new CurrentUser()

            if (allowedRoles.includes(role)) 
             return <WrappedComponent {...props} />
            else 
                return <Page404 />
        }
    }
}