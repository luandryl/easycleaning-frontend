import React from 'react'
import CurrentUser from '../../services/CurrentUser'
import Page404 from '../Page404'
export default function RequireAuthorization(WrappedComponent) {
    return function (props) {
        const user = new CurrentUser()

        if (user)
            return <WrappedComponent {...props} />
        else 
            return <Page404 />
    }
}