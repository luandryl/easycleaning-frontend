import roleTypes from '../config/UserRoleTypes'

export default class CurrentUser {
    constructor () {
        if (localStorage.getItem('user')) {
            const { typeusersec } = JSON.parse(localStorage.getItem('user')).user
            const { company_idcompany } = JSON.parse(localStorage.getItem('user')).user
            this._userRole(typeusersec)
            this.idCompany = company_idcompany
            this.user = JSON.parse(localStorage.getItem('user')).user
        }
    }

    _userRole (type) {
        switch(type) {
            case 0: 
                this.role = roleTypes.ADMIN
            break;
            case 1:
                this.role = roleTypes.MANAGER
            break;
            case 2:
                this.role = roleTypes.EMPLOYEE
            break;
            default:
                this.role = roleTypes.UNAUTHORIZED
            break;
        }
    }
}