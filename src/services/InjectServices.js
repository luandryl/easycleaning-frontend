import { HttpProvider } from './HttpProvider'
import moment from 'moment'

const resolve = (data) => {
	let services = []
	
	data = filter(data)
	data.map(service => {
		services.push(injectServices(service))
	})

	return new Promise((resolve, rejects) => {
		Promise.all(services)
			.then(res => {
				resolve(res, null)
			}).catch(e => {
				rejects(e, null)
			})
	})

}

const filter = (data) => {
	const newData = data.map((serviceArr, i) => {
		let { servicesStatus, services, servicesDuration } = serviceArr

		let serviceInject = services.filter((s, j) => {
			return servicesStatus[j] !== 1
		})

		let servicesDurationInj = servicesDuration.filter((s, j) => {
			return servicesStatus[j] !== 1
		})

		let obj = {
			...
			data[i],
			services: serviceInject,
			servicesDuration: servicesDurationInj
		}
		delete obj.servicesStatus
		return obj
	})

	return newData
}

const injectServices = (service) => {
	return new Promise((resolve, rejects) => {
		Promise.all(loadServiceFromApi(service.services))
			.then(res => {
				
				service = {
					...service,
					services: res
				}
				service.services = service.services.map((s, i) => {
					return { ...s, employeePay: (service.priceHour * service.servicesDuration[i]).toFixed(2) }
				})
				resolve(service, null)
			}).catch(e => {
				rejects(e, null)
			})
	})
}

const loadServiceFromApi = (serviceIdArray) => {
	const promises = serviceIdArray.map(id => {
		return new Promise((resolve, rejects) => {
			HttpProvider.get('service/id/' + id)
				.then(({data}) => { 
					let service = {
						...data[0],
						dateservices: moment(data[0].dateservices).format('MM/DD/YYYY'),
					}
					delete service.client_company_idcompany
					delete service.client_idclient
					resolve(service, null)
				})
				.catch(e => {
					rejects(e, null)
				})
		})
	})

	return promises
}

export const InjectServices = {
	resolve
}

export default InjectServices