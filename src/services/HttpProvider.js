'use-strict'
import axios from 'axios'
import API from '../config/Index'

const Client = axios.create({
  baseURL: API.uri
})

Client.interceptors.response.use((response) => {
  return response
}, error => {
  if (error.response.status === 401 && error.response.data === "Unauthorized") {
    localStorage.removeItem('user')
    window.location = '/?error=timeout'
  }
  return Promise.reject(error);
})

const _getHeaders = () => {
  return JSON.parse(localStorage.getItem('user')) 
    ? { Authorization: `Bearer ${JSON.parse(localStorage.getItem('user')).token.acess_token}` }
    : ''
}

const get = (resource) => {
  return Client.get(API.uri + resource, {headers: _getHeaders()})
}

const post = (resource, data) => {
  return Client.post(API.uri + resource, data, {headers: _getHeaders()})
}

const put = (resource, data) => {
  return Client.put(API.uri + resource, data, {headers: _getHeaders()})
}

const del = (resource) => {
  return Client.delete(API.uri + resource, {headers: _getHeaders()})
}


export const HttpProvider = {
  get,
  post,
  put,
  del
}