import { authorized, unauthorized } from "./types";
import { HttpProvider } from '../../services/HttpProvider'
import Swal from 'sweetalert2'

export const signup = (data) => dispatch => {
    HttpProvider.post('auth/signup/', data)
        .then(res => {
            if(res.status === 200) {
                if(!localStorage.getItem('user')) {
                    localStorage.setItem('user', JSON.stringify(res.data))
                    dispatch({ type: authorized })
                }
            } else {
                dispatch({ type: unauthorized })
            }
        }).catch(e => {
            dispatch({type: unauthorized})
            const msg = e.response.status === 400 ? e.response.data.error : 'Our server have failed!'
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: msg,
              })
        })
}

export const login = (data) => dispatch => {
    let credentials = {
        emailusersec: data.email,
        passwordusersec: data.password
    }
    HttpProvider.post('auth/login/', credentials)
        .then(res => {
            if(res.status === 200) {
                if(!localStorage.getItem('user')) {
                    localStorage.setItem('user', JSON.stringify(res.data))
                    dispatch({ type: authorized })
                }
            } else {
                dispatch({ type: unauthorized })
            }
        }).catch(e => {
            dispatch({type: unauthorized})
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: e.response.data.error,
                confirmButtonText: 'Try again?'
            })
        })
}

export const logout = () => dispatch => {
    localStorage.removeItem('user')
    dispatch({ type: unauthorized })
  }