import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'

import 'antd/dist/antd.css';

import store from './store/store'
import routes from './config/Routes'
import { authorized } from './store/actions/types'

/*
    Authentication
*/
import requireAuthentication from './components/authentication/require_auth';
import noAuthenticaion from './components/authentication/no_auth';
/*
    Current User
*/

import roleTypes from  './config/UserRoleTypes'
/*
    Role Restricions
*/
import authorization from './components/authorization/authorization'
import RequireAuthorization from './components/authorization/require_authorization';

const Menager = authorization([roleTypes.MANAGER])
const Admin = authorization([roleTypes.ADMIN])
const Employee = authorization([roleTypes.EMPLOYEE])

const user = localStorage.getItem('user')

if(user) {
    store.dispatch({ type: authorized })
}

const RouterNodes = routes.map((route, i) => {
    switch(route.role) {
        case roleTypes.ADMIN:
           return <Route 
                path={route.path} 
                exact={true} 
                key={route.id}
                component={
                    route.auth 
                    ? requireAuthentication(RequireAuthorization(Admin(route.component))) 
                    : noAuthenticaion(RequireAuthorization(Admin(route.component)))
                } 
            />
        case roleTypes.MENAGER:
            return <Route 
                path={route.path}
                exact={true}
                key={route.id} 
                component={
                    route.auth 
                    ? requireAuthentication(RequireAuthorization(Menager(route.component))) 
                    : noAuthenticaion(RequireAuthorization(Menager(route.component)))
                } 
            />
        case roleTypes.EMPLOYEE:
            return <Route 
                path={route.path}
                exact={true} 
                key={route.id}
                component={
                    route.auth 
                    ? requireAuthentication(RequireAuthorization(Employee(route.component))) 
                    : noAuthenticaion(RequireAuthorization(Employee(route.component)))
                } 
            />
        default:
            return <Route 
                path={route.path}
                exact={true} 
                key={route.id}
                component={
                    route.auth 
                    ? requireAuthentication(route.component) 
                    : noAuthenticaion(route.component)} 
            />
      }
})

const browserRouter = (
    <BrowserRouter>
        <Switch>
            {RouterNodes}
        </Switch>
    </BrowserRouter>
)

let AppRender = (
    <Provider store={store}>
        {browserRouter}
    </Provider>
)

ReactDOM.render(AppRender, document.getElementById('root'));