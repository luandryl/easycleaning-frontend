import React, { Component } from 'react'
import Default from '../../DefaultPage'
import CurrentUser from '../../../services/CurrentUser'
import { HttpProvider } from '../../../services/HttpProvider'
import { InjectServices } from '../../../services/InjectServices'
import Table from './TableEmployees'
import '../report.css'
import { 
	DatePicker, 
	Spin,
	Input
} from 'antd'


class ReportEmployees extends Component {

	state = {
		isLoading: false,
		employees: [],
		employeesFilter: [],
		dateConsult: {}
	}

	dateHandler = (date) => {
		if (date.length > 0) {
		
			let dateConsult = {
				startDate: date[0].format('MM/DD/YYYY'),
				endDate: date[1].format('MM/DD/YYYY')
			}
			
			this.setState({dateConsult}, () => {
				this.consultService()
			})
			
		}

	}

	filterName = (e) => {
		const { employees } = this.state

		if (e.target.value == '') {
			this.setState({employeesFilter: employees})
		} else {
			let filterEmp = employees.filter(emp => {
				return emp.name.toUpperCase().includes(e.target.value.toUpperCase())
			})
			this.setState({employeesFilter: filterEmp})
		}
	}

	consultService = () => {
		const idCompany = new CurrentUser().idCompany
		const { dateConsult } = this.state
		this.setState({isLoading: true})
		HttpProvider.post('report/employees/' + idCompany, dateConsult)
			.then(({data}) => {
				InjectServices.resolve(data)
					.then(employees => { 
						this.setState({employees, employeesFilter: employees, isLoading: false} ) })
			})
	}


	render () {
		const { RangePicker } = DatePicker
		const loading = (<Spin className="space" tip="Loading..."> </Spin>);
		const { isLoading, employeesFilter } = this.state
		const Search = Input
		return (
			<Default>
				<div className="report-wrapper">
					<h1 className='report-title'> Employees Services Report </h1>
					<div className="report-controls"> 
						<RangePicker 
							size="large" 
							style={{marginBottom: "10px"}} 
							onChange={this.dateHandler}
							format="MM/DD/YYYY"
						/>
						<Search
							size="large"
                        	placeholder="Filter by name"
                        	onChange={this.filterName}
                    	/>
					</div>
					
					{isLoading ? loading : (<Table reloadData={this.consultService} data={employeesFilter} />)}
					
				</div>
			</Default>
		)
	}

}

export default ReportEmployees

