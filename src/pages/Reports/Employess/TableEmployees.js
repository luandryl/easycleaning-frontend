import React, { Component } from "react";
import { HttpProvider } from '../../../services/HttpProvider'
import moment from 'moment'
import Swal from 'sweetalert2'
import '../Clients/table.css'

import { 
	Button,
	Modal,
	Table,
	Spin
} from 'antd'

class TableEmployees extends Component {

	state = {
		employeeId: '',
		employeeName: '',
		isShowModal: false,
		isLoading: false,
		services: [],
		serviceColumns : [{
			title: 'Date',
			dataIndex: 'dateservices',
			key: 'dateservices'
		}, {
			title: 'Pay to Employee',
			dataIndex: 'employeePay',
			key: 'employeePay'
		},{
			title: 'Mark as Paid',
			key: 'services',
			render: (text, record) => (
			  <span>
				<Button size='small' onClick={() => { this.payService(record.idservices) }} >Pay</Button>
			  </span>
			),
		  }
		]
	}

	payService (serviceId) {
		
		const { employeeId, services } = this.state
		this.setState({isLoading: true})
		let payObj = {
			datepayments: moment().format('MM/DD/YYYY'),
			idservicespayments: serviceId,
			idusersecpayments: employeeId
		}
	
		HttpProvider.post('payment/', payObj)
			.then(() => {
				Swal.fire({
					type: 'success',
					title: 'Success',
					text: 'Service Mark as Paid',
					confirmButtonText: 'Ok'
				})
			})
			.then(() => {
				const servicesFilter = services.filter(s => {
					return s.idservices !== serviceId
				})
				this.setState({services: servicesFilter, isLoading:false})
			})
	}

	payAll (employeeData) {
		const { services, id } = employeeData

		if (services.length > 0) {
			this.setState({isLoading: true})

			const servicesPromise = services.map(service => {
				let payObj = {
					datepayments: moment().format('MM/DD/YYYY'),
					idservicespayments: service.idservices,
					idusersecpayments: id
				}
				return HttpProvider.post('payment/', payObj)
			})

			Promise.all(servicesPromise)
				.then (() => {
					Swal.fire({
						type: 'success',
						title: 'Success',
						text: 'Services Mark as Paid',
						confirmButtonText: 'Ok'
					})
				})
				.then(() => {
					this.setState({isLoading: false})
					this.props.reloadData()
				})
		} else {
			Swal.fire({
				type: 'warning',
				title: 'No data',
				text: 'All services and were paid for this dates',
				confirmButtonText: 'Ok'
			})
		}
		
	}

	showModalServices (employeeData) {
		const { services, id, name } = employeeData
		if (services.length > 0) {
			this.setState({isShowModal: true, services, employeeName: name, employeeId: id})
		} else {
			Swal.fire({
				type: 'warning',
				title: 'No data',
				text: 'All services and were paid for this dates',
				confirmButtonText: 'Ok'
			})
		}
	}

	closeModalService = () => {
		this.setState({isShowModal: false})
		this.props.reloadData()
	}

	render () {
		const { data } = this.props
		const { isShowModal, serviceColumns, services, isLoading, employeeName } = this.state
		const loading = (<Spin className="space" tip="Loading..."> </Spin>);
		const dataEl = data.map((e) => {
			return (
				<div className="table--content" key={e.id}>
					<div className="table--top">
					</div>
					<div className="table--cels-wrapper">
						<div className="table--cel" > 
							<span className="table-title" >Name: </span> 
							<label className='table-label' >{e.name}</label>
						</div>
						<div className="table--cel" > 
							<span className="table-title" >Price/Hour: </span> 
							<label className='table-label' > ${e.priceHour}</label>
						</div>
						<div className="table--cel" > 
							<span className="table-title" >Worked Hours: </span> 
							<label className='table-label' > {e.workedHours}H</label>
						</div>
						<div className="table--cel" > 
							<span className="table-title" >Paid Amount:</span> 
							<label className='table-label' > ${e.paidAmount}</label>
						</div>
						<div className="table--cel" > 
							<span className="table-title" >Revenue:</span> 
							<label className='table-label' > ${e.revenue}</label>
						</div>
						<div className="table--cel" >
							<span className="table-title" >Services Details</span>
							<div>
								<Button size="large" onClick={() => { this.showModalServices(e) }} >Open</Button>
								<Button size="large" type="primary" onClick={() => { this.payAll(e) }} >Pay All</Button>
							</div>
						</div>
					</div>
				</div>

			)
		})

		return (
			<div className="table--wrapper" >
				{dataEl}
				<Modal
					title={'Service Details: ' + employeeName}
					visible={isShowModal}
					onCancel={this.closeModalService}
					footer={[
						<Button key="back" onClick={this.closeModalService}>
							Close
						</Button>
					]}
					>
					
					{isLoading ? loading : (
						<Table
							pagination={false}
							columns={serviceColumns}
							dataSource={services}
							rowKey='idservices'
						/>

					) }

				</Modal>
			</div>
		)
	}
}


export default TableEmployees