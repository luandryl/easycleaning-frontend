import React, { Component } from 'react'
import { HttpProvider } from '../../../services/HttpProvider'
import moment from 'moment'
import Swal from 'sweetalert2'
import './table.css'
import { 
	Button,
	Modal,
	Table,
	Spin
} from 'antd'
class TableClients extends Component {

	state = {
		clientName: '',
		isShowModal: false,
		isLoading: false,
		services: [],
		serviceColumns : [{
			title: 'Date',
			dataIndex: 'dateservices',
			key: 'dateservices'
		}, {
			title: 'Price',
			dataIndex: 'priceservices',
			key: 'priceservices'
		},{
			title: 'Mark as Received',
			key: 'services',
			render: (text, record) => (
			  <span>
				<Button size='small' onClick={() => { this.reciveService(record.idservices) }} >Recive</Button>
			  </span>
			),
		  }
		]
	 }

	reciveService (serviceId) {
		const { services } = this.state
		this.setState({isLoading: true})
		let reciveObj = {
			datereceipt: moment().format('MM/DD/YYYY'),
			idservicesreceipt: serviceId
		}
		
		HttpProvider.post('receipt/', reciveObj)
			.then(() => {
				Swal.fire({
					type: 'success',
					title: 'Success',
					text: 'Service Mark as Paid',
					confirmButtonText: 'Ok'
				})
			})
			.then(() => {
				const servicesFilter = services.filter(s => {
					return s.idservices !== serviceId
				})
				this.setState({services: servicesFilter, isLoading:false})
			})

	}

	reciveAll (services) {
		if (services.length > 0) {
			this.setState({isLoading: true})
	
			const serivceAllPromise = services.map(service => {
				let reciveObj = {
					datereceipt: moment().format('MM/DD/YYYY'),
					idservicesreceipt: service.idservices
				}

				return HttpProvider.post('receipt/', reciveObj)
			})
			
			Promise.all(serivceAllPromise)
				.then(() => {
					Swal.fire({
						type: 'success',
						title: 'Success',
						text: 'Service Mark as Paid',
						confirmButtonText: 'Ok'
					})
				})
				.then(() => {
					this.setState({services: [], isLoading:false})
					this.props.reloadData()
				})
		} else {
			Swal.fire({
				type: 'warning',
				title: 'No data',
				text: 'All services and were paid for this dates',
				confirmButtonText: 'Ok'
			})
		}
		
	}

	showModalServices (services, name) {
		if (services.length > 0) {
			this.setState({isShowModal: true, services, clientName: name})
		} else {
			Swal.fire({
				type: 'warning',
				title: 'No data',
				text: 'All services and were paid for this dates',
				confirmButtonText: 'Ok'
			})
		}
	}

	closeModalService = () => {
		this.setState({isShowModal: false})
		this.props.reloadData()
	}

	render () {

		const { data } = this.props
		const { isShowModal, serviceColumns, services, isLoading, clientName } = this.state

		const loading = (<Spin className="space" tip="Loading..."> </Spin>);

		const dataEl = data.map((e) => {
			return (
				<div className="table--content" key={e.id}>
					<div className="table--top">
					</div>
					<div className="table--cels-wrapper">
						<div className="table--cel" > 
							<span className="table-title" >Name: </span> 
							<label className='table-label' >{e.name}</label>
						</div>
						<div className="table--cel" >
							<span className="table-title" >Price/Hour:</span> 
							<label className='table-label' >${e.priceHour}</label>
						</div>

						<div className="table--cel" >
							<span className="table-title" >Price Total:</span> 
							<label className='table-label' >${e.priceTotal}</label>
						</div>

						<div className="table--cel" >
							<span className="table-title" >Recived Amount:</span> 
							<label className='table-label' >${e.receivedAmount}</label>
						</div>
						
						<div className="table--cel" >
							<span className="table-title" >To Recive:</span> 
							<label className='table-label' >${e.toReceiveAmount}</label>
						</div>

						<div className="table--cel" >
							<span className="table-title" >Services Details</span>
							<div>
								<Button size="large"  onClick={() => { this.showModalServices(e.services, e.name) }} >Open</Button>
								<Button size="large" type="primary" onClick={() => { this.reciveAll(e.services) }} >Recive All</Button>
							</div>
						</div>
					</div>
					
				</div>
			)
		})

		return (
			<div className="table--wrapper" >
				{dataEl}
				<Modal
					title={'Service Details: ' + clientName}
					visible={isShowModal}
					onCancel={this.closeModalService}
					footer={[
						<Button key="back" onClick={this.closeModalService}>
							Close
						</Button>
					]}
					>
					
					{isLoading ? loading : (
						<Table
							pagination={false}
							columns={serviceColumns}
							dataSource={services}
							rowKey='idservices'
						/>

					) }

				</Modal>
			</div>
			)
	}

}

export default TableClients