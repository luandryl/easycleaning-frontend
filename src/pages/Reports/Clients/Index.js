import React, { Component } from 'react'
import Default from '../../DefaultPage'
import CurrentUser from '../../../services/CurrentUser'
import { HttpProvider } from '../../../services/HttpProvider'
import { InjectServices } from '../../../services/InjectServices'


import Table from './TableClients'
import '../report.css'

import {
	DatePicker, 
	Spin,
	Input,
} from 'antd'

class ReportClient extends Component {

	state = {
		clients: [],
		clientsFilter: [],
		isLoading: false,
		dateConsult: {}
	}


	dateHandler = (date) => {
		if (date.length > 0) {
	
			let dateConsult = {
				startDate: date[0].format('MM/DD/YYYY'),
				endDate: date[1].format('MM/DD/YYYY')
			}
			this.setState({dateConsult}, () => {
				this.consultService()
			})
		}
		
	}

	consultService = () => {
		const idCompany = new CurrentUser().idCompany
		const { dateConsult } = this.state
		this.setState({isLoading: true})
		HttpProvider.post('report/clients/' + idCompany, dateConsult)
			.then(({data}) => {
				InjectServices.resolve(data)
					.then(clients => { this.setState({clients, clientsFilter: clients, isLoading:false}) })
			})
	}

	filterName = (e) => {
		const { clients } = this.state
		if (e.target.value == '') {
			this.setState({clientsFilter: clients})
		} else {
			let filterCli = clients.filter(cli => {
				return cli.name.toUpperCase().includes(e.target.value.toUpperCase())
			})

			this.setState({clientsFilter: filterCli})
		}
	}

	render () {
		const loading = (<Spin  className="space" tip="Loading..."> </Spin>);
		const { isLoading, clientsFilter } = this.state
		const { RangePicker } = DatePicker
		const { Search } = Input

		return (
			<Default>
				<div className="report-wrapper">
					<h1 className='report-title'> Client Services Report </h1>
					<div className="report-controls"> 
						<RangePicker 
							style={{marginBottom: "10px"}} 
							size="large" 
							onChange={this.dateHandler}
							format="MM/DD/YYYY"
						/>
						<Search
							size="large"
                        	placeholder="Filter by name"
                        	onChange={this.filterName}
                    	/>
					</div>
					
					{isLoading ? loading : (<Table reloadData={this.consultService} data={clientsFilter} />)}
					
				</div>
			</Default>
		)
	}

}

export default ReportClient