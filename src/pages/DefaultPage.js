import React, { Component } from 'react'
import { Button, Icon } from 'antd';

import Nav from '../components/Navigation/Index'
import Content from '../components/Content/Index'

import './dafault.css'

class DefaultPage extends Component {

	state = {
		isOpen: false
	}

	handleClick = e => {
		this.setState({isOpen : !this.state.isOpen})
	}

	render() {
		const { isOpen } = this.state

		const iconMenu = isOpen 
			? ( <Icon style={{color: "#fff", fontSize: "20px"}} type="close" /> ) 
			: ( <Icon style={{color: "#fff", fontSize: "20px"}} type="menu" />  )

		return (
			<div className="app--wrapper">
				<div className="app--content">
					<div className="top--wrapper">
						<span className="menu--top-icon" onClick={this.handleClick}> 
							{iconMenu}
						</span>
						<h1 className="title-top">EasyCleaning</h1>
					</div>
					{isOpen 
						? (
							<Nav isOpen={isOpen} close={this.handleClick} />
						) 
						: (
							<div className="menu-fix">
								<Content >
									{this.props.children}
								</Content>
							</div>
						)
					}
					
				</div>
			</div>
		);
	}
}

export default DefaultPage