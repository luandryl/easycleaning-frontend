import React, { Component } from 'react'
import { 
	Button
} from 'antd'
class ManagersCard extends Component {

	render () {

		const { data } = this.props

		const dataEl = data.map((e, i) => {
			return (
				<div className="table--content" key={e.idusersec}>
					<div className="table--top">
					</div>
					<div className="table--cels-wrapper">
						<div className="table--cel" > 
							<span className="table-title" >Name: </span> 
							<label className='table-label' >{e.nameusersec}</label>
						</div>
						<div className="table--cel" > 
							<span className="table-title" >Email: </span> 
							<label className='table-label' >{e.emailusersec}</label>
						</div>
						<div className="table--cel" > 
							<span className="table-title" >Status: </span> 
							<label className='table-label' >{e.statususersec}</label>
						</div>
						<div className="table--cel" > 
							<span className="table-title" >Action: </span> 
							<span>
								{e.statususersec == 'Active' && (
									<Button type="primary"  onClick={() => {this.props.change(e)}}>Inactive</Button>
								)}
								{e.statususersec == 'Inactive' && (
									<Button onClick={() => {this.props.change(e)}}>Active</Button>
								)}
								<Button type="danger" onClick={()=>{this.props.delete(e.idusersec)}}>Delete</Button>
							</span>
						</div>
					</div>
				</div>
			)
		})

		return (
			<div>
				{dataEl}
			</div>
			
		)
	}

}

export default ManagersCard