import React, { Component } from "react";
import { withRouter } from 'react-router-dom'
import { Input } from 'antd';
import { Spin } from 'antd';
import Swal from 'sweetalert2'
import { HttpProvider } from '../../services/HttpProvider'
import './man.css'
import Default from '../DefaultPage'
import ManagersCard from './ManagersCard'

class ManagersPage extends Component {

	state ={ 
		managers: [],
		managersFilter: [],
		isLoading: true,
	}

	changeStatus = (e) => {
		delete e.passwordusersec
		Swal.fire({
			title: 'Are you sure?',
			text: 'This could block the company to acess the system!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!',
		}).then(result => {
			if (result.value) {
				let user = { ...e, statususersec: e.statususersec =='Active' ? 0 : 1 }
				HttpProvider.put('user/id/' + user.idusersec, user)
					.then(() => {this.setState({isLoading: true})})
					.then(() => {this._loadDataFromApi()})
					.catch(err => console.log(err))
			}
		})


	} 

	handleDelete = (id) => {
		Swal.fire({
			title: 'Are you sure?',
			text: 'You will not be able to reverse this action!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, remove!',
		}).then((result) => {
			if (result.value) {
				this.setState({isLoading: true})
				HttpProvider.del('user/id/' + id)
					.then(() => this._loadDataFromApi())
					.catch(err => console.log(err))
			}
		})
	}

	handleAdd = e => {
		this.props.history.push('/managers/register/')
	}

	_loadDataFromApi () {
		HttpProvider.get('user/managers/')
			.then(({data}) =>  {
				let managers = data.map(item => {
					return item = {...item, statususersec: item.statususersec == 1 ? 'Active': 'Inactive'}
				})
				this.setState({managers, managersFilter: managers})
			})
			.then(() => this.setState({isLoading: false}))
			.catch(e => console.log(e))
	}

	componentWillMount () {
		this._loadDataFromApi()
	}

	filterName = (e) => {
		const { managers } = this.state
		console.log()
		if (e.target.value == '') {
			this.setState({managersFilter: managers})
		} else {
			let filterMan = managers.filter(emp => {
				return emp.nameusersec.toUpperCase().includes(e.target.value.toUpperCase())
			})
			this.setState({managersFilter: filterMan})
		}
	}

	render() {
		const { Search } = Input
		const loading = (<Spin style={{marginTop: "3rem"}} tip="Loading..."> </Spin>);
		const table = (
			<ManagersCard 
				delete={this.handleDelete} 
				change={this.changeStatus} 
				data={this.state.managersFilter} />
		);
		return (
			<Default>
				<div className="report-wrapper">
					<h1 className='report-title'>Managers </h1>
					<div className="report-controls">
					<Search
						style={{marginTop: "15px"}}
						size='large'
						placeholder="Filter by name"
						onChange={this.filterName}
					/>
				</div>
					<div className="managers--wrapper">
						{this.state.isLoading ? loading : table}
					</div>
				</div>
			</Default>
		);
	}

}

export default withRouter(ManagersPage)