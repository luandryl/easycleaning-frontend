import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { HttpProvider } from '../../services/HttpProvider'
import CurrentUser from '../../services/CurrentUser'
import Default from '../DefaultPage'
import Swal from 'sweetalert2'

import {
	Form,
	Input,
	Button,
} from 'antd';

class ProfileEdit extends Component {
	state = {
		confirmDirty: false,
		autoCompleteResult: [],
		user: {}
	  };
	  
	componentWillMount () {

		const { user } = new CurrentUser()
		this.setState({user})
	}
	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFieldsAndScroll((err, data) => {
		  if (!err) {
			const { user } = this.state
			const idCompany = new CurrentUser().idCompany
			let emplooye = {
			  nameusersec: data.name,
			  emailusersec: data.email,
			  passwordusersec: data.password,
			  contactusersec:data.contact,
			  typeusersec: 2,
			  pricehourusersec: user.pricehourusersec,
			  statususersec: 1,
			  ssnusersec:0,
			  company_idcompany: idCompany
			}
			HttpProvider.put('user/id/' + user.idusersec , emplooye)
			  .then(res => {
				if (res.status === 200) {
				  Swal.fire({
					title:'Saved!',
					text:'Profile Updated.',
					type:"success",
					confirmButtonText:'Ok'
				  }).then(() => this.props.history.push('/'))
				} else {
				  Swal({
					title:'Error!',
					text:'An error occured.',
					confirmButtonText:'Ok'
				  })
				}
			  })
			  .catch(e => console.log(e))

		  }
		});
	  };
	
	  handleConfirmBlur = e => {
		const { value } = e.target;
		this.setState({ confirmDirty: this.state.confirmDirty || !!value });
	  };
	
	  compareToFirstPassword = (rule, value, callback) => {
		const { form } = this.props;
		if (value && value !== form.getFieldValue('password')) {
		  callback('Two passwords that you enter is inconsistent!');
		} else {
		  callback();
		}
	  };
	
	  validateToNextPassword = (rule, value, callback) => {
		const { form } = this.props;
		if (value && this.state.confirmDirty) {
		  form.validateFields(['confirm'], { force: true });
		}
		callback();
	  };
	  
	  onChange = e => {
		const { value } = e.target;
		const reg = /^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/;
		if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
		  this.props.onChange(value);
		}
	  };
	  

	render () {
		const { getFieldDecorator } = this.props.form;
		const { user } = this.state
		const formItemLayout = {
		labelCol: {
			xs: { span: 24 },
			sm: { span: 8 },
		},
		wrapperCol: {
			xs: { span: 24 },
			sm: { span: 16 },
		},
		};
		const tailFormItemLayout = {
		wrapperCol: {
			xs: {
			span: 24,
			offset: 0,
			},
			sm: {
			span: 16,
			offset: 8,
			},
		},
		};
		
		return (
			<Default>
				<h1 style={{textAlign: "center"}} className='report-title'>Edit Profile </h1>
				<div style={{padding: "1rem"}} className="register--wrapper space">
						
					<Form {...formItemLayout} onSubmit={this.handleSubmit}>
					  <Form.Item label="Name">
						  {getFieldDecorator('name', {
							  initialValue: user.nameusersec,
							  rules: [{ required: true, message: 'Please input your name!', whitespace: true }],
						  })(<Input onChange={this.handleName} size="large" />)}
					  </Form.Item>
						<Form.Item label="Email">
						{getFieldDecorator('email', {
							initialValue: user.emailusersec,
							rules: [
							{
								type: 'email',
								message: 'The input is not valid E-mail!',
							},
							{
								required: true,
								message: 'Please input your E-mail!',
							},
							],
						})(<Input size="large" />)}
						</Form.Item>
						<Form.Item label="Password" hasFeedback>
						{getFieldDecorator('password', {
							rules: [
							{
								required: true,
								message: 'Please input your password!',
							},
							{
								validator: this.validateToNextPassword,
							},
							],
						})(<Input.Password size="large" />)}
						</Form.Item>
						<Form.Item label="Confirm Password" hasFeedback>
						{getFieldDecorator('confirm', {
							rules: [
							{
								required: true,
								message: 'Please confirm your password!',
							},
							{
								validator: this.compareToFirstPassword,
							},
							],
						})(<Input.Password size="large" onBlur={this.handleConfirmBlur} />)}
						</Form.Item>
						<Form.Item label="Phone Number">
						  {getFieldDecorator('contact', {
							initialValue: user.contactusersec,
							rules: [{ required: true, message: 'Please input your number!', whitespace: true }],
						  })(<Input size="large" />)}
						</Form.Item>

						<Form.Item {...tailFormItemLayout}>
						<div className="button-wrapper">
						  <Button block size="large" type="primary" htmlType="submit">
							  Save
						  </Button>
						</div>
						</Form.Item>
					</Form>
				</div>
			</Default>
		)
	}
}
const ProfEdit = Form.create({name: 'edit_profile_employee'})(ProfileEdit)
export default withRouter(ProfEdit)