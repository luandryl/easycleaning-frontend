import React, { Component } from "react";
import { withRouter } from 'react-router-dom'
import { Table } from 'antd';
import { Spin, Button } from 'antd';
import Swal from 'sweetalert2'
import CurrentUser from '../../services/CurrentUser'
import { HttpProvider } from '../../services/HttpProvider'
import Default from '../DefaultPage'

import './emp.css'

class EmployeesPage extends Component {

    state ={ 
        emplooyes: [],
        isLoading: true,
        columns: [
          {
            title: 'Name',
            dataIndex: 'nameusersec',
            key: 'nameusersec',
          },
          {
            title: 'Email',
            dataIndex: 'emailusersec',
            key: 'emailusersec',
          },
          {
            title: 'Price / Hour',
            dataIndex: 'pricehourusersec',
            key: 'addpricehourusersecress',
          },
          {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
              <span>
                <Button size="default" block onClick={(e) => this.handleEdit(e, record.idusersec)} >Edit</Button>
                <div className="btn-space--table"> </div>
                <Button size="small" block type="danger" onClick={(e) => this.handleDelete(e, record.idusersec)} >Delete</Button>
              </span>
            ),
          },
        ]
    }

    handleEdit = (e, id) => {
      console.log(id)
      this.props.history.push('/employees/edit/' + id)
    }

    handleDelete = (e, id) => {
      e.preventDefault()
      Swal.fire({
          title: 'Are you sure?',
          text: 'You will not be able to reverse this action!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, remove!',
      }).then((result) => {
          if (result.value) {
            this.setState({isLoading: true})
            HttpProvider.del('user/id/' + id)
              .then(() => this._loadDataFromApi())
              .catch(err => console.log(err))
          }
      })
    }

    handleAdd = e => {
        this.props.history.push('/employees/register')
    }

    _loadDataFromApi = () => {
      const idCompany = new CurrentUser().idCompany
		  HttpProvider.get('user/employees/' + idCompany)
        .then(res => { this.setState({emplooyes: res.data} ) })
        .then(() => this.setState({isLoading: false}))
        .catch(e => console.log(e))
    }

    componentWillMount () {
      this._loadDataFromApi()
    }

    render() {
        const { columns, emplooyes, isLoading } = this.state
        const loading = (<Spin style={{marginTop: "3rem"}} tip="Loading..."> </Spin>);
        const table = (
            <Table 
              columns={columns} 
              rowKey="idusersec" 
              pagination={false} 
              dataSource={emplooyes} 
            />
        );
        return (
            <Default>
              <div className="report-wrapper">
					      <h1 className='report-title'>Employees </h1>
					        <div className="report-controls">
                    <Button block type="primary" size="large" onClick={this.handleAdd} >+ Employee</Button>
                  </div>
                  <div style={{paddingTop: "0", marginBottom: "3rem"}}  className="emp--table">
                    {isLoading ? loading : table}
                  </div>
                </div>
            </Default>
        );
    }

}

export default withRouter(EmployeesPage)