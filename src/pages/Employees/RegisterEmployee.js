import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Default from '../DefaultPage'
import Swal from 'sweetalert2'
import { HttpProvider } from '../../services/HttpProvider'

import CurrentUser from '../../services/CurrentUser'

import {
    Form,
    Input,
    Button,
} from 'antd';
    
class RegisterEmployee extends Component {

    state = {
        confirmDirty: false,
        autoCompleteResult: [],
      };
    
      handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, data) => {
          if (!err) {
            const idCompany = new CurrentUser().idCompany
            let emplooye = {
              nameusersec: data.name,
              emailusersec: data.email,
              passwordusersec:data.password,
              contactusersec:data.contact,
              typeusersec: 2,
              pricehourusersec: data.price,
              statususersec: 1,
              ssnusersec:0,
              company_idcompany: idCompany
            }
            HttpProvider.post('user/', emplooye)
              .then(res => {
                if (res.status === 200 && res.data === 'INSERT') {
                  Swal.fire({
                    title:'Saved!',
                    text:'A new Employee was register.',
                    confirmButtonText:'Ok'
                  }).then(() => this.props.history.push('/employees/'))
                } else {
                  Swal({
                    title:'Error!',
                    text:'An error occured.',
                    confirmButtonText:'Ok'
                  })
                }
              })
              .catch(e => console.log(e))
          }
        });
      };
    
      handleConfirmBlur = e => {
        const { value } = e.target;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
      };
    
      compareToFirstPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && value !== form.getFieldValue('password')) {
          callback('Two passwords that you enter is inconsistent!');
        } else {
          callback();
        }
      };
    
      validateToNextPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && this.state.confirmDirty) {
          form.validateFields(['confirm'], { force: true });
        }
        callback();
      };
      
      change = e => {
        const { value } =  e.target
        const reg = /[0-9]*\.?[0-9]*/;
        console.log(value.match(reg)[0])
    
        return value.match(reg)[0]
      };
    
    render () {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
        },
        };
        const tailFormItemLayout = {
        wrapperCol: {
            xs: {
            span: 24,
            offset: 0,
            },
            sm: {
            span: 16,
            offset: 8,
            },
        },
        };

        return (
            <Default>
                <h1 style={{textAlign: "center"}} className='report-title'>Register Emplooyes </h1>
                <div style={{padding: "1rem"}} className="register--wrapper space">
                        
                    <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                      <Form.Item label="Name">
                          {getFieldDecorator('name', {
                              rules: [{ required: true, message: 'Please input your name!', whitespace: true }],
                          })(<Input size="large" />)}
                      </Form.Item>
                        <Form.Item label="Email">
                        {getFieldDecorator('email', {
                            rules: [
                            {
                                type: 'email',
                                message: 'The input is not valid E-mail!',
                            },
                            {
                                required: true,
                                message: 'Please input your E-mail!',
                            },
                            ],
                        })(<Input size="large" />)}
                        </Form.Item>
                        <Form.Item label="Password" hasFeedback>
                        {getFieldDecorator('password', {
                            rules: [
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                            {
                                validator: this.validateToNextPassword,
                            },
                            ],
                        })(<Input.Password size="large" />)}
                        </Form.Item>
                        <Form.Item label="Confirm Password" hasFeedback>
                        {getFieldDecorator('confirm', {
                            rules: [
                            {
                                required: true,
                                message: 'Please confirm your password!',
                            },
                            {
                                validator: this.compareToFirstPassword,
                            },
                            ],
                        })(<Input.Password size="large" onBlur={this.handleConfirmBlur} />)}
                        </Form.Item>
                        <Form.Item label="Phone Number">
                          {getFieldDecorator('contact', {
                            rules: [{ required: true, message: 'Please input your number!', whitespace: true }],
                          })(<Input size="large" />)}
                        </Form.Item>

                        <Form.Item label="Price/Hour">
                          {getFieldDecorator('price', {
                            getValueFromEvent: this.change,
                            rules: [{ required: true, message: 'Only numbers and dot allowed', whitespace: false }],
                          })(<Input
                            prefix="$" 
                            size="large" />)}
                        </Form.Item>
                        
                        <Form.Item {...tailFormItemLayout}>
                        <div className="button-wrapper">
                          <Button block size="large" type="primary" htmlType="submit">
                              Register
                          </Button>
                        </div>
                        </Form.Item>
                    </Form>
                </div>
            </Default>   
        );
    }
}
const EmployeeRegister = Form.create({name: 'reg_employee'})(RegisterEmployee)
export default withRouter(EmployeeRegister)