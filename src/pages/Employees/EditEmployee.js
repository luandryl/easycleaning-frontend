import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { HttpProvider } from '../../services/HttpProvider'
import CurrentUser from '../../services/CurrentUser'
import Default from '../DefaultPage'
import Swal from 'sweetalert2'

import {
	Form,
	Input,
	Button,
	Spin
} from 'antd';


class EditProfile extends Component {
	state = {
		user: {},
		isLoading: true,
		priceH: ''
	}

	handleConfirmBlur = e => {
        const { value } = e.target;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
      };
    
      handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFieldsAndScroll((err, data) => {
		  if (!err) {
			const { user } = this.state
			const idCompany = new CurrentUser().idCompany
			let emplooye = {
			  nameusersec: user.nameusersec,
			  emailusersec: user.emailusersec,
			  contactusersec: user.contactusersec,
			  typeusersec: 2,
			  pricehourusersec: data.price,
			  statususersec: 1,
			  ssnusersec:0,
			  company_idcompany: idCompany
			}
			
			HttpProvider.put('user/id/' + user.idusersec , emplooye)
			  .then(res => {
				if (res.status === 200) {
				  Swal.fire({
					title:'Saved!',
					text:'Profile Updated.',
					type:"success",
					confirmButtonText:'Ok'
				  }).then(() => this.props.history.push('/employees/'))
				} else {
				  Swal({
					title:'Error!',
					text:'An error occured.',
					confirmButtonText:'Ok'
				  })
				}
			  })
				.catch(e => console.log(e))
				
		  }
		});
	  };

      change = e => {
				const { value } =  e.target
				const reg = /[0-9]*\.?[0-9]*/;
				console.log(value.match(reg)[0])

				return value.match(reg)[0]
      };

	componentDidMount () {
		const { id } = this.props.match.params
		
		console.log(id)
		HttpProvider.get('user/id/' + id)
			.then(({data}) =>{
				this.setState({user:data[0], isLoading: false}, () => console.log(this.state.user) )
			})
	}

	render() {
		const loading = (<Spin style={{marginTop: "3rem"}} tip="Loading..."> </Spin>);
		const { isLoading, user } = this.state
		const { getFieldDecorator } = this.props.form;
		
		const formItemLayout = {
		labelCol: {
			xs: { span: 24 },
			sm: { span: 8 },
		},
		wrapperCol: {
			xs: { span: 24 },
			sm: { span: 16 },
		},
		};
		const tailFormItemLayout = {
		wrapperCol: {
			xs: {
			span: 24,
			offset: 0,
			},
			sm: {
			span: 16,
			offset: 8,
			},
		},
		};
		return (
			<Default>
				{isLoading 
					? loading 
					: (
						<div>
							<h1 style={{textAlign: "center"}} className='report-title'>Edit Employee </h1>
							<div className="register--wrapper space">
									
								<Form {...formItemLayout} onSubmit={this.handleSubmit}>
								<Form.Item label="Name">
									{getFieldDecorator('name', {
										initialValue: user.nameusersec
									})(<Input size="large" disabled />)}
								</Form.Item>
									<Form.Item label="Email">
									{getFieldDecorator('email', {
										initialValue: user.emailusersec
									})(<Input size="large" disabled />)}
									</Form.Item>
									
									<Form.Item label="Phone Number">
									{getFieldDecorator('contact', {
										initialValue: user.contactusersec,
									})(<Input size="large" disabled />)}
									</Form.Item>
									<Form.Item label="Price/Hour">
										{getFieldDecorator('price', {
											initialValue: user.pricehourusersec,
											getValueFromEvent: this.change,
											rules: [{ required: true, message: 'Only numbers and dot allowed', whitespace: false }],
										})(<Input
											prefix="$" 
											size="large" />)}
									</Form.Item>
                        
			
									<Form.Item {...tailFormItemLayout}>
									<div className="button-wrapper">
									<Button block size="large" type="primary" htmlType="submit">
										Save
									</Button>
									</div>
									</Form.Item>
								</Form>
							</div>
						</div>
					)}
			</Default>
		)
	}
}
const EditProf = Form.create({name: 'edit_employee'})(EditProfile)
export default withRouter(EditProf)