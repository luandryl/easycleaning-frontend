import React, { Component } from "react";
import { withRouter } from 'react-router-dom'
import { Table, Divider, Tag } from 'antd';
import { Spin, Button } from 'antd';
import Swal from 'sweetalert2'
import { HttpProvider } from '../../services/HttpProvider'
import CurrentUser from '../../services/CurrentUser'

import Default from '../DefaultPage'

class ClientPage extends Component {

    state ={ 
        clients: [],
        isLoading: true,
        columns: [{
          title: 'Name',
          dataIndex: 'nameclient',
          key: 'nameclient',
        },
        {
          title: 'Street',
          dataIndex: 'streetclient',
          key: 'streetclient',
        },
        {
          title: 'No.',
          dataIndex: 'numberstreetclient',
          key: 'numberstreetclient',
        },
        {
          title: 'Price / Hour',
          dataIndex: 'pricehourclient',
          key: 'pricehourclient',
        },
        {
          title: 'Action',
          key: 'action',
          render: (text, record) => (
            <span>
              <Button block onClick={(e) => this.handleEdit(e, record.idclient)} >Edit</Button>
              <div className="btn-space--table"> </div>
              <Button block type="danger" onClick={(e) => this.handleDelete(e, record.idclient)} >Delete</Button>
            </span>
          )
        }
      ]
    }

    handleEdit = (e, id) => {
      this.props.history.push('/clients/edit/' + id)
    }

    handleDelete = (e, id) => {
      console.log(id)
      e.preventDefault()
      Swal.fire({
          title: 'Are you sure?',
          text: 'You will not be able to reverse this action!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, remove!',
      }).then((result) => {
          if (result.value) {
            this.setState({isLoading: true})
            HttpProvider.del('client/' + id)
                .then(() => this._loadDataFromApi())
                .catch(err => console.log(err))
          }
      })

  }

    handleAdd = e => {
        this.props.history.push('/clients/register')
    }

    _loadDataFromApi () {
        const idCompany = new CurrentUser().idCompany
        HttpProvider.get('client/all/' + idCompany)
        .then(res => this.setState({clients: res.data}))
        .then(() => this.setState({isLoading: false}))
        .catch(e => console.log(e))
    }

    componentWillMount () {
      this._loadDataFromApi()
    }

    render() {
        
        const loading = (<Spin style={{marginTop: "3rem"}} tip="Loading..."> </Spin>);
        const {columns, clients, isLoading} = this.state
        const table = (
            <Table 
              columns={columns} 
              rowKey="idclient" 
              pagination={false} 
              dataSource={clients} 
            />
        );
        return (
            <Default>
              <div className="report-wrapper">
					        <h1 className='report-title'>Clients </h1>
					        <div className="report-controls">
                    <Button block type="primary" size="large" onClick={this.handleAdd} >+ Client</Button>
                  </div>
                  <div style={{paddingTop: "0"}} className="emp--table">
                    {isLoading ? loading : table}
                  </div>
                </div>
            </Default>
        );
    }

}

export default withRouter(ClientPage)