import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Default from '../DefaultPage'
import Swal from 'sweetalert2'
import { HttpProvider } from '../../services/HttpProvider'
import CurrentUser from '../../services/CurrentUser'
import './register.css'

import {
    Form,
    Input,
    Button,
  } from 'antd';
  
  
class RegisterClients extends Component {

    state = {
        confirmDirty: false,
        autoCompleteResult: [],
      };
    
      handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, data) => {
          const idCompany = new CurrentUser().idCompany
          if (!err) {
            let client = {
              nameclient: data.name,
              contactclient: data.contact,
              emailclient: data.email,
              streetclient: data.street,
              numberstreetclient: data.number,
              zipcodeclient: data.zip_code,
              complementclient: data.complement,
              cityclient: data.city,
              pricehourclient: data.price,
              company_idcompany: idCompany,
            }
            HttpProvider.post('client/', client)
              .then(res => {
                console.log(res)
                if (res.status === 200 && res.data === 'INSERT') {
                  Swal.fire({
                    title:'Saved!',
                    text:'A new Client was register.',
                    confirmButtonText:'Ok'
                  }).then(() => this.props.history.push('/clients/'))
                } else {
                  Swal({
                    title:'Error!',
                    text:'An error occured.',
                    confirmButtonText:'Ok'
                  })
                }
              })
              .catch(e => console.log(e))

            
          }
        });
      };
    
      handleConfirmBlur = e => {
        const { value } = e.target;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
      };
    
      compareToFirstPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && value !== form.getFieldValue('password')) {
          callback('Two passwords that you enter is inconsistent!');
        } else {
          callback();
        }
      };
    
      validateToNextPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && this.state.confirmDirty) {
          form.validateFields(['confirm'], { force: true });
        }
        callback();
      };

      change = e => {
        const { value } =  e.target
        const reg = /[0-9]*\.?[0-9]*/;
        console.log(value.match(reg)[0])
    
        return value.match(reg)[0]
      };
    
    
    render () {
        const { getFieldDecorator } = this.props.form;
        
        const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
        },
        };
        const tailFormItemLayout = {
        wrapperCol: {
            xs: {
            span: 24,
            offset: 0,
            },
            sm: {
            span: 16,
            offset: 8,
            },
        },
        };
        return (
            <Default>
                <h1 style={{textAlign: "center"}} className='report-title'>Register Clients </h1>
                <div className="register--wrapper space">
                    <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                      <Form.Item label="Name">
                          {getFieldDecorator('name', {
                              rules: [{ required: true, message: 'Please input the client name!', whitespace: true }],
                          })(<Input size="large" />)}
                      </Form.Item>
                      <Form.Item label="Phone Number">
                          {getFieldDecorator('contact', {
                            rules: [{ required: true, message: 'Please input the client phone number!', whitespace: true }],
                          })(<Input size="large" />)}
                        </Form.Item>
                        <Form.Item label="Email">
                          {getFieldDecorator('email', {
                              rules: [
                              {
                                  type: 'email',
                                  message: 'The input is not valid E-mail!',
                              },
                              {
                                  required: false,
                                  whitespace: true,
                              },
                              ],
                          })(<Input size="large" />)}
                          </Form.Item>
                        <Form.Item label="Street">
                          {getFieldDecorator('street', {
                              rules: [{ required: true, message: 'Please input the client street!', whitespace: true }],
                          })(<Input size="large" />)}
                        </Form.Item>
                        <Form.Item label="Street Number">
                          {getFieldDecorator('number', {
                              rules: [{ required: true, message: 'Please input the client street number!', whitespace: true }],
                          })(<Input size="large" />)}
                        </Form.Item>
                        <Form.Item label="Complement ">
                            {getFieldDecorator('complement', {
                                rules: [{ required: false, whitespace: true }],
                            })(<Input size="large" />)}
                        </Form.Item>
                        <Form.Item label="Zip Code">
                          {getFieldDecorator('zip_code', {
                              rules: [{ required: false, whitespace: true }],
                          })(<Input size="large" />)}
                        </Form.Item>
                        <Form.Item label="City">
                            {getFieldDecorator('city', {
                                rules: [{ required: false, whitespace: true }],
                            })(<Input size="large" />)}
                        </Form.Item>
                        <Form.Item label="Price/Hour">
                          {getFieldDecorator('price', {
                            getValueFromEvent: this.change,
                            rules: [{ required: true, message: 'Only numbers and dot allowed', whitespace: false }],
                          })(<Input
                            prefix="$" 
                            size="large" />)}
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                          <div className="button-wrapper">
                            <Button block size="large" type="primary" htmlType="submit">
                                Register
                            </Button>
                          </div>
                        </Form.Item>
                    </Form>
                </div>
            </Default>    
        );
    }
}
const ClientRegister = Form.create({name: 'reg_manager'})(RegisterClients)
export default withRouter(ClientRegister)