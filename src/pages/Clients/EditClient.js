import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { HttpProvider } from '../../services/HttpProvider'
import CurrentUser from '../../services/CurrentUser'
import Default from '../DefaultPage'
import Swal from 'sweetalert2'

import {
	Form,
	Input,
	Button,
	Spin
} from 'antd';

class EditClient extends Component {

    state = {
        user: {},
        confirmDirty: false,
        autoCompleteResult: [],
        isLoading: true,
      };
    
      handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, data) => {
          const idCompany = new CurrentUser().idCompany
          if (!err) {
            let client = {
              nameclient: data.name,
              contactclient: data.contact,
              emailclient: data.email,
              streetclient: data.street,
              numberstreetclient: data.number,
              zipcodeclient: data.zip_code,
              complementclient: data.complement,
              cityclient: data.city,
              pricehourclient: data.price,
              company_idcompany: idCompany,
            }
            HttpProvider.put('client/' + this.state.user.idclient, client)
              .then(res => {
                console.log(res)
                if (res.status === 200) {
                  Swal.fire({
                    title:'Saved!',
                    text:'Client Updated.',
                    confirmButtonText:'Ok'
                  }).then(() => this.props.history.push('/clients/'))
                } else {
                  Swal({
                    title:'Error!',
                    text:'An error occured.',
                    confirmButtonText:'Ok'
                  })
                }
              })
              .catch(e => console.log(e))
            
          }
        });
      };
    
      handleConfirmBlur = e => {
        const { value } = e.target;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
      };
    
      compareToFirstPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && value !== form.getFieldValue('password')) {
          callback('Two passwords that you enter is inconsistent!');
        } else {
          callback();
        }
      };
    
      validateToNextPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && this.state.confirmDirty) {
          form.validateFields(['confirm'], { force: true });
        }
        callback();
      };

      change = e => {
        const { value } =  e.target
        const reg = /[0-9]*\.?[0-9]*/;
        console.log(value.match(reg)[0])
    
        return value.match(reg)[0]
      };
    
      componentDidMount () {
        const { id } = this.props.match.params
        
        HttpProvider.get('client/id/' + id)
          .then(({data}) =>{
            this.setState({user:data[0], isLoading: false} )
          })
    }
    
    render () {
        const { getFieldDecorator } = this.props.form;
        const { isLoading, user } = this.state
        const loading = (<Spin style={{marginTop: "3rem"}} tip="Loading..."> </Spin>);
        const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
        },
        };
        const tailFormItemLayout = {
        wrapperCol: {
            xs: {
            span: 24,
            offset: 0,
            },
            sm: {
            span: 16,
            offset: 8,
            },
        },
        };
        return (
            <Default>
                {isLoading 
                ? loading :(
                    <div>
                        <h1 style={{textAlign: "center"}} className='report-title'>Edit Client </h1>
                        <div className="register--wrapper space">
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                            <Form.Item label="Name">
                                {getFieldDecorator('name', {
                                    initialValue: user.nameclient,
                                    rules: [{ required: true, message: 'Please input the client name!', whitespace: true }],
                                })(<Input size="large" />)}
                            </Form.Item>
                            <Form.Item label="Phone Number">
                                {getFieldDecorator('contact', {
                                    initialValue: user.contactclient,
                                    rules: [{ required: true, message: 'Please input the client Phone Number!', whitespace: true }],
                                })(<Input size="large" />)}
                                </Form.Item>
                                <Form.Item label="Email">
                                {getFieldDecorator('email', {
                                    initialValue: user.emailclient,
                                    rules: [
                                    {
                                        type: 'email',
                                        message: 'The input is not valid E-mail!',
                                    },
                                    {
                                        required: false,
                                        whitespace: true,
                                    },
                                    ],
                                })(<Input size="large" />)}
                                </Form.Item>
                                <Form.Item label="Address">
                                {getFieldDecorator('street', {
                                    initialValue: user.streetclient,
                                    rules: [{ required: true, message: 'Please input the client Address!', whitespace: true }],
                                })(<Input size="large" />)}
                                </Form.Item>
                                <Form.Item label="Address Number">
                                {getFieldDecorator('number', {
                                    initialValue: user.numberstreetclient,
                                    rules: [{ required: true, message: 'Please input the client Address Number!', whitespace: true }],
                                })(<Input size="large" />)}
                                </Form.Item>
                                <Form.Item label="Complement ">
                                    {getFieldDecorator('complement', {
                                        initialValue: user.complementclient,
                                        rules: [{ required: false, whitespace: true }],
                                    })(<Input size="large" />)}
                                </Form.Item>
                                <Form.Item label="Zip Code">
                                {getFieldDecorator('zip_code', {
                                    initialValue: user.zipcodeclient,
                                    rules: [{ required: false, whitespace: true }],
                                })(<Input size="large" />)}
                                </Form.Item>
                                <Form.Item label="City">
                                    {getFieldDecorator('city', {
                                        initialValue: user.cityclient,
                                        rules: [{ required: false, whitespace: true }],
                                    })(<Input size="large" />)}
                                </Form.Item>
                                <Form.Item label="Price/Hour">
                                {getFieldDecorator('price', {
                                    initialValue: user.pricehourclient,
                                    getValueFromEvent: this.change,
                                    rules: [{ required: true, message: 'Only numbers and dot allowed', whitespace: false }],
                                })(<Input
                                    prefix="$" 
                                    size="large" />)}
                                </Form.Item>
                                <Form.Item {...tailFormItemLayout}>
                                <div className="button-wrapper">
                                    <Button block size="large" type="primary" htmlType="submit">
                                        Register
                                    </Button>
                                </div>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                )}
                
            </Default>
        )
    }
}
const EditCli = Form.create({name: 'edit_client'})(EditClient)
export default withRouter(EditCli)