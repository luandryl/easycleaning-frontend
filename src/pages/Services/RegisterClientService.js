import React, { Component } from 'react';
import { HttpProvider } from '../../services/HttpProvider'
import { Spin, Table, Button, Input, Icon } from 'antd'
import Swal from 'sweetalert2' 
import CurrentUser from '../../services/CurrentUser'

class RegisterClientService extends Component {

    state = {
        isLoading: true,
        clients: [],
        clientsFilter: [],
        selectedRowKey: [],
        columns: [{
            title: 'Name',
            dataIndex: 'nameclient',
            key: 'nameclient',
          },
          {
            title: 'Street',
            dataIndex: 'streetclient',
            key: 'streetclient',
          },
          {
            title: 'Number',
            dataIndex: 'numberstreetclient',
            key: 'numberstreetclient',
          },
          {
            title: 'Complement',
            dataIndex: 'complementclient',
            key: 'complementclient',
          }
        ]
    }

    componentWillMount () {
        const idCompany = new CurrentUser().idCompany
        HttpProvider.get('client/all/' + idCompany)
            .then(({data}) => { this.setState({ clients: data, clientsFilter: data } )})
            .then(() => { this.setState({isLoading: false}) })
    }

    onSelectChange = selectedRowKey => {
        this.setState({ selectedRowKey })
    }
    
    clientHandler (e) {
        e.preventDefault()
        const { selectedRowKey } = this.state

        if (selectedRowKey.length > 0) {
            this.props.getClient(selectedRowKey)
            this.props.nextStep()
        } else {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Select one Client for this service',
                confirmButtonText: 'Try again!'
            })
        }
    }

    filterName = (e) => {
		const { clients } = this.state
		if (e.target.value == '') {
			this.setState({clientsFilter: clients})
		} else {
			let filterCli = clients.filter(cli => {
				return cli.nameclient.toUpperCase().includes(e.target.value.toUpperCase())
			})

			this.setState({clientsFilter: filterCli})
		}
	}

    render () {
        const { isLoading, clientsFilter, columns, selectedRowKey } = this.state
        const loading = (<Spin tip="Loading..."> </Spin>);

        const rowSelection = {
            selectedRowKey,
            onChange: this.onSelectChange,
            type: 'radio'
        }

        const { Search } = Input

        const content = (
            <div className="report-wrapper">
				<h1 className='report-title'>Select Client </h1>
				<div className="report-controls">
                    <div className="fill"> 
                        <Button 
                            icon="left" 
                            size="large" 
                            onClick={(e) => {this.props.prevStep()}} 
                        >
                            Previous
                        </Button>
                        <Button 
                            size="large" 
                            onClick={(e) => {this.clientHandler(e)}} 
                            type="primary"
                        >
                            Next
                            <Icon type="right" />
                        </Button>
                    </div>
                    <Search
                        placeholder="Filter by name"
                        onChange={this.filterName}
                        size="large"
                        style={{marginTop: "10px", marginBottom: "25px"}}
                    />
				</div>
				<div style={{padding: "0px", margin: "0px"}} className="register--content" >
                    <Table 
                        pagination={false} 
                        rowSelection={rowSelection} 
                        columns={columns} 
                        rowKey="idclient" 
                        dataSource={clientsFilter} 
                    />
				</div>
			</div>
        )
        
        return (
            isLoading ? loading : content
        )
    }

}

export default RegisterClientService