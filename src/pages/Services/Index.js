import React, { Component } from 'react'
import Default from '../DefaultPage'
import RegisterTeam from './RegisterTeam'
import RegisterClientService from './RegisterClientService'
import DateService from './DateService'
import OutService from './OutService'

import Swal from 'sweetalert2' 

import './services.css'
import {
	Spin
} from 'antd';


class Services extends Component {

	constructor (props) {
		super(props)
		this.state = {
			isLoading: false,
			current: 0,
			employees: [],
			client: [],
			dateService: {},
			teamId: '',
			isResume: false
		}

		this.prev = this.prev.bind(this)
		this.next = this.next.bind(this)
		this.employeesHandler = this.employeesHandler.bind(this)
		this.clientHandler = this.clientHandler.bind(this)
		this.dateHandler = this.dateHandler.bind(this)
	}

	next () {
		const current = this.state.current + 1
		this.setState({current})
	}

	prev () {
		const current = this.state.current - 1
		this.setState({current})
	}
	
	employeesHandler (employees) {
		this.setState({employees})
	}

	clientHandler (clientId) {
		this.setState({clientId})
	}

	dateHandler(dateService) {
		this.setState({dateService}, () => {
			const {employees, clientId, dateService} = this.state

			const resumeService = {
				employees, 
				clientId, 
				dateService
			}
			if (!localStorage.getItem('service')) {
				localStorage.setItem('service', JSON.stringify(resumeService))
				this.setState({isResume: true})
			}
		})
	}


	cancelOut = () => {
		if (localStorage.getItem('service')) {
			localStorage.removeItem('service')
		}
		this.setState({isResume: false, current: 0})
	}

	componentWillMount () {
		if (localStorage.getItem('service')) {
			this.setState({isResume: true})
		}
	}

	render () {
		const { current, isLoading, isResume } = this.state
		const loading = (<Spin tip="Loading..."> </Spin>);
		const content = (
				<div style={{marginBottom: "3rem"}}>
					{current == 0 && (
						<RegisterTeam getTeam={this.employeesHandler} nextStep={this.next}/>
					)}
					{current == 1 && (
						<RegisterClientService getClient={this.clientHandler} nextStep={this.next} prevStep={this.prev} />
					)}
					{current == 2 && (
						<DateService getDate={this.dateHandler} nextStep={this.next} prevStep={this.prev} />
					)}
				</div>
		);
		return (
			<Default>
				<div className="service--wrapper">
					{isResume ? (<OutService cancel={this.cancelOut} />) : isLoading ? loading: content}
				</div>
			</Default>
		)
	}
}

export default Services