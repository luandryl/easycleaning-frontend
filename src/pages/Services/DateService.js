import React, { Component } from 'react'
import moment from 'moment'
import 'rc-time-picker/assets/index.css';
import './services.css'
import { Button, Typography, DatePicker, TimePicker, Icon } from 'antd'

class DateService extends Component {

	state = {
		dateService: moment(),
		entry: moment(),
	}

	onChange (value) {
		console.log(value && value.format('hh:mm:ss A'))
	}

	dateHandler = dateService => { this.setState({dateService}) }
	handleEntry = value => { this.setState({entry: value }) }
	
	timeHandler(e) {
		e.preventDefault()
		const { entry, dateService } = this.state
		const date = {
			dateService: dateService.format('MM/DD/YYYY'),
			entry: entry.format('hh:mm:ss A')
		}
		this.props.getDate(date)
	}

	render () {
		const { Text } = Typography
		const inServiceConfig = {
			backgroundColor: "#00D255", 
			borderColor: "#00D255", 
			padding: "0 40px", 
			fontSize: "20px"
		}
		return (
			<div className="report-wrapper">
				<h1 className='report-title'>In Service </h1>
				<div style={{paddingTop: "0"}} className="register--content" >
					<div className="date-wrapper">
						<div className="date">
							<Text strong>Date of service</Text> <br />
							<DatePicker size="large" defaultValue={moment()} format="MM/DD/YYYY" disabled />
						</div>
						<div className='date-item'>
							<Text strong>Entry Hour</Text> <br />
							<TimePicker
								use12Hours
								size="large"
								showSecond={false}
								format="hh:mm A"
								defaultValue={moment()}
								defaultOpenValue={moment()}
								onChange={this.handleEntry}
							/>
						</div>
					</div>
				</div>
				<div className="fill">
					<Button  
						size="large" 
						onClick={(e) => {this.props.prevStep()}}
						icon="left"
					>
						Previous
					</Button>
					<Button
						style={inServiceConfig}
						size="large" 
						onClick={(e) => {this.timeHandler(e)}} 
						type="primary"
					>
						In
						<Icon type="usergroup-add" />
					</Button>
				</div>
			</div>
		)
	}
}

export default DateService