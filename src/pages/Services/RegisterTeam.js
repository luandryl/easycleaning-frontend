import React, {Component} from 'react'
import { HttpProvider } from '../../services/HttpProvider'
import { Spin, Table, Button, Input, Icon } from 'antd'
import Swal from 'sweetalert2' 
import CurrentUser from '../../services/CurrentUser'
class RegisterTeam extends Component {  

	state = {
		employees: [],
		employeesFilter: [],
		isLoading: true,
		columns: [{
			title: 'Name',
			dataIndex: 'nameusersec',
			key: 'nameusersec',
		  },
		  {
			title: 'Email',
			dataIndex: 'emailusersec',
			key: 'emailusersec',
		  }
		],
		selectRowKeys: []
	}

	componentWillMount () {
		const idCompany = new CurrentUser().idCompany
		HttpProvider.get('user/employees/' + idCompany)
			.then(({data}) => { this.setState({employees: data, employeesFilter: data})})
			.then(() => { this.setState({isLoading: false}) })
	}

	onSelectChange = selectedRowKeys => {
		this.setState({ selectedRowKeys });
		console.log(this.state.selectedRowKeys)
	}

	teamHandler (e) {
		e.preventDefault()
		const { selectedRowKeys } = this.state
		

		
		if (selectedRowKeys) {
			this.props.getTeam(selectedRowKeys)
			if(selectedRowKeys.length >= 1)
				this.props.nextStep()
			else{
				Swal.fire({
					type: 'error',
					title: 'Oops...',
					text: 'Select at least one Employee for the service',
					confirmButtonText: 'Try again!'
				})
			}
			console.log(selectedRowKeys.length);
		} else{
			Swal.fire({
				type: 'error',
				title: 'Oops...',
				text: 'Select at least one Employee for the service',
				confirmButtonText: 'Try again!'
			})
		}
	}
		

	filterName = (e) => {
		const { employees } = this.state

		if (e.target.value == '') {
			this.setState({employeesFilter: employees})
		} else {
			let filterEmp = employees.filter(emp => {
				return emp.nameusersec.toUpperCase().includes(e.target.value.toUpperCase())
			})
			this.setState({employeesFilter: filterEmp})
		}
	}

	render () {

		const { isLoading, employeesFilter, columns, selectedRowKeys } = this.state
		const { Search } = Input
		const loading = (<Spin tip="Loading..."> </Spin>);
		const rowSelection = {
			selectedRowKeys,
			onChange: this.onSelectChange
		}

		const content = (
			<div className="report-wrapper">
				<h1 className='report-title'>Select team </h1>
				<div className="report-controls">
					<Button 
						size="large" 
						onClick={(e) => {this.teamHandler(e)}} 
						type="primary"
					>
						Next
						<Icon type="right" />
					</Button>
					<Search
						placeholder="Filter by name"
						onChange={this.filterName}
						size="large"
						style={{marginTop: "10px", marginBottom: "25px"}}
					/>
				</div>
				<div style={{paddingTop: "0"}} className="register--content" >
					<Table 
						pagination={false} 
						rowSelection={rowSelection} 
						columns={columns} 
						rowKey="idusersec" 
						dataSource={employeesFilter} 
					/>
				</div>
			</div>
		)
		
		return (
			isLoading ? loading : content
		)
	}
}

export default RegisterTeam