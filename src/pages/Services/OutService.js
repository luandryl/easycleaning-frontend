import React, { Component } from 'react'
import { HttpProvider } from '../../services/HttpProvider'
import CurrentUser from '../../services/CurrentUser'
import moment from 'moment'
import Swal from 'sweetalert2'

import './services.css'
import {
	Spin,
	Divider, 
	Typography, 
	DatePicker,
	TimePicker,
	Button,
	Icon,
	Modal
} from 'antd';


class OutService extends Component {

	state = {
		employees: [],
		client: {},
		dateService: {},
		isLoading: true,
		exit: moment(),
		teamId: '',
		isModalCheckOutShow: false
		
	}

	handleExit = value => { this.setState({exit: value }) }

	openModal = () => { this.setState({isModalCheckOutShow: true}) }
	closeModal = () => { this.setState({isModalCheckOutShow: false}) }

	_loadEmployees (employeesIds) {

		return new Promise((resolve, rejects) => {
			const employeesPromise = employeesIds.map(id => {
				return HttpProvider.get('user/id/' + id)
			})
			Promise.all(employeesPromise).then((res) => {
				let employeesData = []
				res.map(({data}) => {
					let employee = {
						name: data[0].nameusersec,
						email: data[0].emailusersec,
						id: data[0].idusersec
					}
					employeesData.push(employee)
				})
				resolve(employeesData, null)
			}).catch(e => {
				rejects(e, null)
			})
		})
	}

	_loadClient (clientId) {
		return new Promise((resolve, rejects) => {
			HttpProvider.get('client/id/' + clientId)
				.then(({data}) => {
					let client = {
						id: data[0].idclient,
						name: data[0].nameclient,
						street: data[0].streetclient,
						complement: data[0].complementclient
					}
					resolve(client, null)
				}).catch(e => {
					rejects(e, null)
				})
		})
	}

	_saveService () {
		const { dateService, exit } = this.state
		let date = {
			...dateService,
			exit: exit.format('hh:mm:ss A')
		}
		const { employees, clientId } = JSON.parse(localStorage.getItem('service'))

		this.setState({ isLoading: true })

		let team = {
			usersec_idusersec: employees[0]
		}

		HttpProvider.post('team/', team)
			.then(({data}) => { this.setState({ teamId:data[0].idteam}) })
			.then(() => {
				if (employees.length > 1) {
					const { teamId } = this.state
					delete employees[0]
					employees.map(employee => {
						let team = {
							idteam: teamId,
							usersec_idusersec: employee
						}
						HttpProvider.post('team/addUser/', team)
							.catch(e => {
								console.log(e)
							})
					})
				}
			})
			.then(() => {
				const idCompany = new CurrentUser().idCompany
				let service = {
					dateservices: date.dateService,
		  			entryhourservices: date.entry,
		  			exithourservices:date.exit,
		  			client_idclient: clientId[0],
		  			client_company_idcompany: idCompany,
		  			idteam: this.state.teamId
				}
				HttpProvider.post('service/', service)
					.then(() => {
							this.setState({isLoading: false})
							this.props.cancel()
						})
					
			})
	}

	checkOut (e) {
		this.closeModal()
		Swal.fire({
			title: 'Are you sure?',
			text: 'This will check out the service!',
			type: 'success',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then(result => {
			if (result.value) {
				this._saveService()
			}
		})
	}

	cancelCheckOut () {
		Swal.fire({
			title: 'Are you sure?',
			text: 'This will cancel the service check out!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!',
		}).then(result => {
			if (result.value) {
				this.props.cancel()
			}
		})
	}

	componentWillMount () {
		if (localStorage.getItem('service')) {
			const { employees, clientId, dateService } = JSON.parse(localStorage.getItem('service'))
			Promise.all([this._loadEmployees(employees), this._loadClient(clientId)])
				.then(res => {
					this.setState({employees: res[0], client: res[1], dateService, isLoading: false})
				})
		}
	}

	render () {
		const loading = (<Spin tip="Loading..."> </Spin>);
		const { Text } = Typography
		const { isLoading, client, employees, dateService, isModalCheckOutShow } = this.state

		const inServiceConfig = {
			backgroundColor: "#00D255", 
			borderColor: "#00D255", 
			padding: "0 40px", 
			fontSize: "20px"
		}

		const ModalCheckOut = (
			<Modal
				title="Service Check Out"
				visible={isModalCheckOutShow}
				onOk={this.closeModal}
				onCancel={this.closeModal}
				footer={[
					<Button 
						key="cancel"
						size="large" 
						onClick={(e) => {this.closeModal()}}
						icon="close"
						type="danger"
					>
						Cancel
					</Button>,
					<Button
						key="submit"
						style={inServiceConfig}
						size="large" 
						onClick={(e) => {this.checkOut(e)}} 
						type="primary"
					>
						Check Out
						<Icon type="usergroup-add" />
					</Button>	
				]}
				>
				<div className='date-item'>
					<Text strong>Exit Hour</Text> <br />
					<TimePicker
						use12Hours
						size="large"
						showSecond={false}
						format="hh:mm A"
						onChange={this.handleExit}
						defaultValue={moment()}
					/>
				</div>
			</Modal>
		)

		const clientView = (
			<div className="table--content" key={client.id}>
					<h1 style={{
						fontSize: "1.5rem", 
						paddingLeft: "1rem", 
						paddingTop: "1rem", 
						margin: 0
					}} 
					className='report-title'>
						Client 
					</h1>
					<div style={{paddingTop: ".1rem"}} className="table--cels-wrapper">
						<div className="table--cel" > 
							<span className="table-title" >Name: </span> 
							<label className='table-label' >{client.name}</label>
						</div>
						<div className="table--cel" > 
							<span className="table-title" >Street: </span> 
							<label className='table-label' >{client.street}</label>
						</div>
						<div className="table--cel" > 
							<span className="table-title" >Complement: </span> 
							<label className='table-label' >{client.complement}</label>
						</div>
					</div>
			</div>
		)

		const employeesView = (
			<div className="table--content">
					<h1 style={{
						fontSize: "1.5rem", 
						paddingLeft: "1rem", 
						paddingTop: "1rem", 
						margin: 0
					}} 
					className='report-title'>
						Employees
					</h1>
					<div style={{paddingTop: ".5rem"}} className="table--cels-wrapper">

						{employees.map(e => {
							return (
								<div key={e.id}>
									<div className="table--cel" > 
										<span className="table-title" >Name: </span> 
										<label className='table-label' >{e.name}</label>
									</div>
									<Divider />
								</div>
							)
						})}
						
					</div>
			</div>
		)

		const dateView = (
			<div className="table--content">
				<h1 style={{
						fontSize: "1.5rem", 
						paddingLeft: "1rem", 
						paddingTop: "1rem", 
						margin: 0
					}} 
					className='report-title'>
						Date/Time
					</h1>
				<div style={{paddingLeft: "1rem", paddingBottom: "1rem"}}>
					<div className="date">
						<Text strong>Date of service</Text> <br />
						<DatePicker size="large" defaultValue={moment(dateService.dateService, "MM/DD/YYYY")} format="MM/DD/YYYY" disabled />
					</div>
					<div className='date-item'>
						<Text strong>Entry Hour</Text> <br />
						<TimePicker
							use12Hours
							size="large"
							showSecond={false}
							format="hh:mm A"
							defaultValue={moment(dateService.entry, "hh:mm A")}
							disabled
						/>
					</div>
				</div>
				
			</div>
		)

		const view = (
			<div>
				{clientView}
				{employeesView}
				{dateView}
				<div style={{marginBottom: "5rem"}} className="fill">
					<Button  
						size="large" 
						onClick={(e) => {this.cancelCheckOut(e)}}
						icon="close"
						type="danger"
					>
						Cancel
					</Button>
					<Button
						style={inServiceConfig}
						size="large" 
						onClick={(e) => {this.openModal(e)}} 
						type="primary"
					>
						Exit Service
						<Icon type="usergroup-add" />
					</Button>
				</div>
			</div>
		)
		return (
			<div className="report-wrapper">
				<h1 className='report-title'>Service Check Out</h1>
				<div className="table--wrapper" >
					{isLoading ? loading : view}
					{isModalCheckOutShow ? ModalCheckOut : ''}
				</div>
			</div>
		)
	}

}

export default OutService