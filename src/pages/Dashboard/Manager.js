import React, { Component } from 'react'
import Default from '../DefaultPage'
import PieChart from 'react-minimal-pie-chart';
import { HttpProvider } from '../../services/HttpProvider'
import CurrentUser from '../../services/CurrentUser'

import { 
	Spin
} from 'antd'

import './dash.css'

class Dashboard extends Component {

	state = {
		data: {},
		dataValues: {},
		isLoading: false
	}

	cleanConsult () {
		this.setState({
			data: {},
			isLoaded: false
		})
	}

	componentWillMount () {
		const idCompany = new CurrentUser().idCompany
		this.setState({isLoading: true})
		HttpProvider.post('graph/' + idCompany)
			.then(({data}) => {
				const totalAmount = Number(data.toReceive) + Number(data.toPay)
				let dataNorm = {
					toPay: (data.toPay/totalAmount) * 100,
					toReceive: (data.toReceive/totalAmount) * 100
				}
				this.setState({dataValues: data, data: dataNorm, isLoading: false, isLoaded: true })
			} )
	}
	render () {
		const { isLoading, data, dataValues } = this.state
		const loading = (<Spin className="space" tip="Loading..."> </Spin>)
		const { toPay, toReceive } = data

		const graph = ( 
				<div className="graph--wrapper">
					<PieChart
						data={[
							{ title: 'Recive', value: toReceive ? parseInt(toReceive) : 0, color: '#0AC400' },
							{ title: 'Pay', value: toPay ? parseInt(toPay) : 0 , color: '#FE0006' }
						]}
						label={true}
						labelStyle={{
							fontSize: '4px',
							fontFamily: 'sans-serif',
							fill: '#121212'
						  }}
						radius={42}
						labelPosition={102}
					/>
				</div>
		 )

		return (
			<Default>
				<div className="dashboard--wrapper">
					<div className="report-wrapper">
						<h1 className='report-title'> Report Gains </h1>
						<div className="graph--info"> 
							<div className="graph-detail">
								<div className="pay"> </div>
								<label>Pay: {dataValues.toPay ? "$" + dataValues.toPay : '' } </label>
							</div>
							<div className="graph-detail" > 
								<div className="receive"> </div>
								<label>Receive: {dataValues.toReceive ? "$" + dataValues.toReceive : '' } </label>
							</div>
						</div>
						{isLoading ? loading:  graph }
					</div>
				</div>
			</Default>
		);
	}
}

export default Dashboard