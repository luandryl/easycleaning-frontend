import React, { Component } from 'react'
import { Form, Icon, Input, Button } from 'antd';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import Swal from 'sweetalert2'

import { login } from '../../store/actions/authAction'

import './auth.css'

class Login extends Component {

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            this.props.login(values)
          }
        })
      }
    componentWillMount () {
        var query = window.location.search.substring(1);
        if (query.split("=")[0] === 'error' && query.split("=")[1] === 'timeout'){
            console.log('swal')
            Swal.fire({
                title:'Opss!',
                text:'Your session has expired! Please login again.',
                type: 'warning',
                confirmButtonText:'Ok'
              })
        }
    }
    render () {
        const { getFieldDecorator } = this.props.form;
        return (
            <div className="login-wrapper align--center">
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                    {getFieldDecorator('email', {
                            rules: [
                            {
                                type: 'email',
                                message: 'The input is not valid E-mail!',
                            },
                            {
                                required: true,
                                message: 'Please input your E-mail!',
                            },
                          ],
                    })(
                        <Input
                        size="large"
                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder="Email"
                        />,
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please input your Password!' }],
                    })(
                        <Input
                        size="large"
                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        type="password"
                        placeholder="Password"
                        />,
                    )}
                    </Form.Item>
                    <Form.Item>
                    <div className="options">
                        <Link className="login-form-forgot" to="/forgot/">
                            Forgot password?
                        </Link>
                    </div>
                    <Button type="primary" size="large" htmlType="submit" className="login-form-button">
                        Login
                    </Button>
                    Or <Link to="/signup/">register now!</Link>
                    </Form.Item>
                </Form>
            </div>
        );
    }
}

Login.prototypes = {
    login: PropTypes.func.isRequired
}
  
const loginForm = Form.create({name: 'normal_login'})(Login)
export default connect(null, { login })(loginForm)

