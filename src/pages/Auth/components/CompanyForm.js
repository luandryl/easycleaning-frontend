import React, { Component } from "react"
import '../auth.css'

import {
    Form,
    Input,
    Button,
  } from 'antd';
  

class Company extends Component {

    state = {
        confirmDirty: false,
      };
    
      handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, data) => {
          if (!err) {
            let company = {
              namecompany: data.name,
              streetcompany: data.street,
              numbercompany: data.number,
              complementcompany: data.complement,
              zipcompany: data.zip_code,
              citycompany: data.city,
              contactcompany: data.tel,
              emailcompany: data.email
            }

            this.props.saveCompanyState(company)
          }
        });
      };
    
      handleConfirmBlur = e => {
        const { value } = e.target;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
      };
    
      compareToFirstPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && value !== form.getFieldValue('password')) {
          callback('Two passwords that you enter is inconsistent!');
        } else {
          callback();
        }
      };
    
      validateToNextPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && this.state.confirmDirty) {
          form.validateFields(['confirm'], { force: true });
        }
        callback();
      };

    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
        },
        wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                span: 24,
                offset: 0,
                },
                sm: {
                span: 16,
                offset: 8,
                },
            },
        };
        return (
            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <Form.Item label="Company Name">
                        {getFieldDecorator('name', {
                            rules: [{ required: true, message: 'Please input the Company Name!', whitespace: true }],
                        })(<Input size="large" />)}
                    </Form.Item>
                    <Form.Item label="Company Street ">
                        {getFieldDecorator('street', {
                            rules: [{ required: true, message: 'Please input the Company Address!', whitespace: true }],
                        })(<Input  size="large" />)}
                    </Form.Item>

                    <Form.Item label="Company Number ST">
                        {getFieldDecorator('number', {
                            rules: [{ required: false, whitespace: true }],
                        })(<Input size="large" />)}
                    </Form.Item>
                    <Form.Item label="Company Complement">
                        {getFieldDecorator('complement', {
                            rules: [{ required: false, whitespace: true }],
                        })(<Input size="large" />)}
                    </Form.Item>
                    <Form.Item label="Zip Code">
                        {getFieldDecorator('zip_code', {
                            rules: [{ required: false, whitespace: true }],
                        })(<Input size="large" />)}
                    </Form.Item>
                    <Form.Item label="Company City">
                        {getFieldDecorator('city', {
                            rules: [{ required: false, whitespace: true }],
                        })(<Input size="large" />)}
                    </Form.Item>
                    <Form.Item label="Company Phone Number">
                        {getFieldDecorator('tel', {
                            rules: [{ required: false, whitespace: true }],
                        })(<Input size="large" />)}
                    </Form.Item>
                    <Form.Item label="Company E-mail">
                    {getFieldDecorator('email', {
                        rules: [
                        {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                        },
                        {
                            required: false,
                            whitespace: true,
                        },
                        ],
                    })(<Input size="large" />)}
                    </Form.Item>
                    
                    <Form.Item {...tailFormItemLayout}>
                        <Button size="large" style={{lineHeight: "48px", marginRight:"10px"}} icon="left" onClick={() => this.props.prev()}>
                            Previus
                        </Button>
                        <Button size="large"  icon="right" type="primary" htmlType="submit">
                            Finish
                        </Button>
                    </Form.Item>
                </Form>
        )
    }

}
const CompanyForm = Form.create({name: 'company_form'})(Company)
export default CompanyForm