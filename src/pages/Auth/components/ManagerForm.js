import React, { Component } from "react"
import { Link, withRouter } from 'react-router-dom'
import '../auth.css'

import {
    Form,
    Input,
    Button,
  } from 'antd';
  

class Manager extends Component {

    state = {
        confirmDirty: false,
      };
    
      handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, data) => {
          if (!err) {
            let manager = {
              nameusersec: data.name,
              passwordusersec: data.password,
              emailusersec: data.email,
              contactusersec:data.contact,
              typeusersec:1,
              pricehourusersec:null,
              ssnusersec:1,
              statususersec: 1
            }
            this.props.saveManagerState(manager)
          }
        });
      };
    
      handleConfirmBlur = e => {
        const { value } = e.target;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
      };
    
      compareToFirstPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && value !== form.getFieldValue('password')) {
          callback('Two passwords that you enter is inconsistent!');
        } else {
          callback();
        }
      };
    
      validateToNextPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && this.state.confirmDirty) {
          form.validateFields(['confirm'], { force: true });
        }
        callback();
      };
    
   

    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
        },
        wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                span: 24,
                offset: 0,
                },
                sm: {
                span: 16,
                offset: 8,
                },
            },
        };
        return (
            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <Form.Item label="Name">
                        {getFieldDecorator('name', {
                            rules: [{ required: true, message: 'Please input your name!', whitespace: true }],
                        })(<Input size="large" />)}
                    </Form.Item>
                    <Form.Item label="Email">
                    {getFieldDecorator('email', {
                        rules: [
                        {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            message: 'Please input your E-mail!',
                        },
                        ],
                    })(<Input size="large" />)}
                    </Form.Item>
                    <Form.Item label="Password" hasFeedback>
                    {getFieldDecorator('password', {
                        rules: [
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                        {
                            validator: this.validateToNextPassword,
                        },
                        ],
                    })(<Input.Password size="large" />)}
                    </Form.Item>
                    <Form.Item label="Confirm Password" hasFeedback>
                    {getFieldDecorator('confirm', {
                        rules: [
                        {
                            required: true,
                            message: 'Please confirm your password!',
                        },
                        {
                            validator: this.compareToFirstPassword,
                        },
                        ],
                    })(<Input.Password size="large" onBlur={this.handleConfirmBlur} />)}
                    </Form.Item>
                    
                    
                    <Form.Item label="Phone Number">
                        {getFieldDecorator('contact', {
                        rules: [{ required: true, message: 'Please input your number!', whitespace: true }],
                        })(<Input size="large" />)}
                    </Form.Item>
                    
                    <Form.Item {...tailFormItemLayout}>
                      <div className="button-wrapper">
                        <Button  
                          style={{lineHeight: "48px", marginRight:"10px"}} 
                          size="large" 
                          >
                            <Link to="/"> Cancel </Link>
                        </Button>
                        <Button 
                          icon="right" 
                          size="large" 
                          type="primary" 
                          htmlType="submit">
                            Next 
                        </Button>
                      </div>
                    </Form.Item>
                </Form>
        )
    }

}
const ManagerForm = Form.create({name: 'manager_form'})(Manager)
export default withRouter(ManagerForm)