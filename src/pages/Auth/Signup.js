import React, { Component } from "react"
import ManagerForm from './components/ManagerForm'
import CompanyForm from './components/CompanyForm'

import { HttpProvider } from '../../services/HttpProvider'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { signup } from '../../store/actions/authAction'

import './auth.css'

import { 
    Steps, 
    Icon
} from 'antd';

class Signup extends Component {

    constructor (props) {
        super(props)
        this.state = {
            current: 0,
            manager: {},
            company: {}
        }

        this.managerDataHandler = this.managerDataHandler.bind(this)
        this.companyDataHandler = this.companyDataHandler.bind(this)
        this.prev = this.prev.bind(this)
    }

    managerDataHandler (man) {
        this.setState({manager: man})
        this.next()
    }

    companyDataHandler (data) {
        this.setState({company: data}, () => {
            this._saveData()
        })
    }

    _saveData () {
        let { manager, company } = this.state
        HttpProvider.post('company/', company)
            .then(res => {
                if (res.status === 200) {
                    manager = { ...manager, company_idcompany: res.data[0].idcompany}
                    this.props.signup(manager)
                }
            }).catch(e => {
                console.log(e)
            })
    }

    next () {
        const current = this.state.current + 1
        this.setState({current})
    }

    prev () {
        const current = this.state.current - 1
        this.setState({current})
    }

    render() {
        const { current } = this.state
        const { Step } = Steps

        return (
            <div className="signup-content align--center">
                <h1 className="login-title">Signup</h1>
                <div className="signup-wrapper">
                    <Steps className="stepper-head" current = {current}>
                        <Step  title="Company Owner Info"icon={<Icon type="user" />} />
                        <Step  title="Company Registration" icon={<Icon type="solution" />} />
                    </Steps>
                    <div className="steps-content">
                    
                        {current === 0 && (
                            <ManagerForm saveManagerState={this.managerDataHandler} />
                        )}
                        {current === 1 && (
                            <CompanyForm prev={this.prev} saveCompanyState={this.companyDataHandler} />
                        )}
                    </div>
                </div>
          </div>
        )
    }

}

Signup.prototypes = {
    signup: PropTypes.func.isRequired
}

export default connect(null, {signup})(Signup) 