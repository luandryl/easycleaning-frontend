import React, { Component } from 'react'

import Login from './Login'
import './auth.css'

class Auth extends Component {

    render () {
        return (
            <div className="auth-wrapper align--center">
                <h1 className="login-title">EasyCleaning</h1>
                <Login />
            </div>
        )
    }

}

export default Auth