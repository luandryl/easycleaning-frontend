import React, { Component }  from 'react'
import { Form, Icon, Input, Button, Spin } from 'antd';
import { Link } from 'react-router-dom'
import { HttpProvider } from '../../services/HttpProvider'

import Swal from 'sweetalert2'
class Forgot extends Component {

	state = {
		isLoading: false
	}

	handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
			
			this.setState({isLoading: true})
			HttpProvider.post('resetPassword/forgot/', values)
				.then(({data}) => {
					if (data.msg !== "User not found!") {
						Swal.fire({
							title: 'Email Sent',
							text: "Please check your email!",
							type: 'success',
							confirmButtonText: 'Ok!'
						})
					} else {
						Swal.fire({
							title: data.msg,
							text: "Your email was not register!",
							type: 'warning',
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'Ok!',
							footer: "<a href='/signup/'>Register? </a>"
						})
					}

				})
				.then(() => {
					this.setState({isLoading: false})
				})
          }
        })
      }

	render () {
		const { getFieldDecorator } = this.props.form;
		const { isLoading } = this.state
		const loading = (<Spin style={{marginTop: "3rem"}} tip="Loading..."> </Spin>);

		const form = (
			<Form onSubmit={this.handleSubmit} className="login-form">
				<Form.Item>
				{getFieldDecorator('email', {
						rules: [
						{
							type: 'email',
							message: 'The input is not valid E-mail!',
						},
						{
							required: true,
							message: 'Please input your E-mail!',
						},
					],
				})(
					<Input
					size="large"
					prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
					placeholder="Email"
					/>,
				)}
				</Form.Item>
				
				<Form.Item>
				<Button type="primary" size="large" htmlType="submit" className="login-form-button">
					Send
				</Button>
				<div style={{marginTop: "1rem", display: "flex", flexFlow: "column", justifyContent: "center"}} className="options">
					<Link style={{lineHeight: "50px"}} to="/">Login</Link>
				</div>
				
				</Form.Item>
			</Form>)

		return ( 
			<div className="auth-wrapper align--center">
				<h1 className="login-title">EasyCleaning</h1>
				<div className="login-wrapper align--center">
					<h1 style={{fontSize: "1.5rem"}} className="login-title">Forgot Passoword</h1>
					{isLoading ? loading : form}
				</div>
			</div>
		) 
	}
}
const ForgotForm = Form.create({name: 'forgot'})(Forgot)
export default ForgotForm