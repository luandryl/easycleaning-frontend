import React, { Component }  from 'react'
import { Form, Spin, Input, Button } from 'antd';
import { withRouter } from 'react-router-dom'
import { HttpProvider } from '../../services/HttpProvider'
import Swal from 'sweetalert2'

class Reset extends Component {
	state = {
		isLoading: false,
		confirmDirty: false,
		autoCompleteResult: [],
	};

	handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
			this.setState({isLoading: true})
			const { id, token } = this.props.match.params
			const data = {
				id,
				token,
				password: values.password
			}

			HttpProvider.post('resetPassword/reset/', data)
				.then(({data}) => {
					Swal.fire({
						title: data.msg,
						type: 'success',
						confirmButtonText: 'Ok!'
					})
				})
				.then(() => {
					this.setState({isLoading: false})
					this.props.history.push('/')
				})
			
          }
        })
      }

	handleConfirmBlur = e => {
		const { value } = e.target;
		this.setState({ confirmDirty: this.state.confirmDirty || !!value });
	  };
	
	  compareToFirstPassword = (rule, value, callback) => {
		const { form } = this.props;
		if (value && value !== form.getFieldValue('password')) {
		  callback('Two passwords that you enter is inconsistent!');
		} else {
		  callback();
		}
	  };
	
	  validateToNextPassword = (rule, value, callback) => {
		const { form } = this.props;
		if (value && this.state.confirmDirty) {
		  form.validateFields(['confirm'], { force: true });
		}
		callback();
	  };
	  
	  onChange = e => {
		const { value } = e.target;
		const reg = /^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/;
		if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
		  this.props.onChange(value);
		}
	  };

	componentDidMount () {
		const { id, token } = this.props.match.params
		if (!id && !token) {
			this.props.history.push('/')
		}
	}

	render () {
		const { getFieldDecorator } = this.props.form;
		const { isLoading } = this.state
		const loading = (<Spin style={{marginTop: "3rem"}} tip="Loading..."> </Spin>);

		const form = (
				<Form onSubmit={this.handleSubmit} className="login-form">
						<Form.Item label="Password" hasFeedback>
							{getFieldDecorator('password', {
								rules: [
								{
									required: true,
									message: 'Please input your password!',
								},
								{
									validator: this.validateToNextPassword,
								},
								],
							})(<Input.Password size="large" />)}
							</Form.Item>
							<Form.Item label="Confirm Password" hasFeedback>
							{getFieldDecorator('confirm', {
								rules: [
								{
									required: true,
									message: 'Please confirm your password!',
								},
								{
									validator: this.compareToFirstPassword,
								},
								],
							})(<Input.Password size="large" onBlur={this.handleConfirmBlur} />)}
							</Form.Item>
						<Form.Item>
						<Button type="primary" size="large" htmlType="submit" className="login-form-button">
							Send
						</Button>
						
						</Form.Item>
					</Form>
		)

		return (
			<div className="auth-wrapper align--center">
				<h1 className="login-title">EasyCleaning</h1>
				<div className="login-wrapper align--center">
					<h1 style={{fontSize: "1.5rem"}} className="login-title">Reset Passoword</h1>
					{isLoading ? loading : form}
				</div>
			</div>
		)
	}
}
const ResetP = Form.create({name: 'reset_pass'})(Reset)

export default withRouter(ResetP)