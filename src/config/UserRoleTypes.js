const UserRoleTypes = {
    UNAUTHORIZED: 'un',
    ADMIN: 'admin',
    MANAGER: 'manager',
    EMPLOYEE: 'employee',
    CLIENT: 'client'
}

export default UserRoleTypes
