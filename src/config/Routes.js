/*
    Routes
*/
import Auth from '../pages/Auth/Index'
import Signup from '../pages/Auth/Signup'
import DashboardMan from '../pages/Dashboard/Manager'
import EmpRegister from '../pages/Employees/RegisterEmployee'
import ManIndex from '../pages/Managers/Index'
import EmpIndex from '../pages/Employees/Index'
import CliIndex from '../pages/Clients/Index'
import CliRegister from '../pages/Clients/RegisterClients'
import ServicesIndex from '../pages/Services/Index'
import CLientReport from '../pages/Reports/Clients/Index'
import EmpReport from '../pages/Reports/Employess/Index'
import ProfileEdit from '../pages/Employees/ProfileEdit'
import EditEmployee from '../pages/Employees/EditEmployee'
import ProfileManEdit from '../pages/Managers/ProfileManEdit'
import EditClient from '../pages/Clients/EditClient'
import Forgot from '../pages/Forgot/Forgot'
import Reset from '../pages/Forgot/Reset'
import ProfAdmin from '../pages/Admin/ProfileAdmin'

/*
    RoleTypes
*/
import roleTypes from  './UserRoleTypes'

const routes = [{
    id: 0,
    path: '/',
    name: 'Login',
    component: Auth,
    isMenuList: false,
    auth: false,
    role: null
}, {
    id: 1,
    path: '/home/admin/',
    name: 'Home',
    component: ManIndex,
    isMenuList: true,
    auth: true,
    role: roleTypes.ADMIN
},{
    id: 2,
    path: '/home/manager/',
    name: 'Home',
    component: DashboardMan,
    isMenuList: true,
    auth: true,
    role: roleTypes.MANAGER
},{
    id: 3,
    path: '/home/employee/',
    name: 'Home',
    component: ServicesIndex,
    isMenuList: true,
    auth: true,
    role: roleTypes.EMPLOYEE
},{
    id: 4,
    path: '/employees/register/',
    name: 'Register Employee',
    component: EmpRegister,
    isMenuList: false,
    auth: true,
    role: roleTypes.MANAGER
},{
    id: 7,
    path: '/employees/',
    name: 'Employees',
    component: EmpIndex,
    isMenuList: true,
    auth: true,
    role: roleTypes.MANAGER
},{
    id: 8,
    path: '/clients/',
    name: 'Clients',
    component: CliIndex,
    isMenuList: true,
    auth: true,
    role: roleTypes.MANAGER
},{
    id: 9,
    path: '/clients/register/',
    name: 'Register Clients',
    component: CliRegister,
    isMenuList: false,
    auth: true,
    role: roleTypes.MANAGER
},{
    id: 10,
    path: '/signup/',
    name: 'Sign Up',
    component: Signup,
    isMenuList: false,
    auth: false,
    role: null
},{
    id: 11,
    path: '/reports/clients',
    name: 'Report Clients',
    component: CLientReport,
    isMenuList: true,
    auth: true,
    role: roleTypes.MANAGER
},{
    id: 12,
    path: '/reports/employees',
    name: 'Report Employees',
    component: EmpReport,
    isMenuList: true,
    auth: true,
    role: roleTypes.MANAGER
},{
    id: 13,
    path: '/employee/profile',
    name: 'Profile',
    component: ProfileEdit,
    isMenuList: true,
    auth: true,
    role: roleTypes.EMPLOYEE
},{
    id: 14,
    path: '/employees/edit/:id',
    name: 'Edit Employee',
    component: EditEmployee,
    isMenuList: false,
    auth: true,
    role: roleTypes.MANAGER
},{
    id: 15,
    path: '/manager/profile',
    name: 'Profile',
    component: ProfileManEdit,
    isMenuList: true,
    auth: true,
    role: roleTypes.MANAGER
},{
    id: 16,
    path: '/clients/edit/:id',
    name: 'Edit Client',
    component: EditClient,
    isMenuList: false,
    auth: true,
    role: roleTypes.MANAGER
},{
    id: 17,
    path: '/forgot/',
    name: 'Forgot Password',
    component: Forgot,
    isMenuList: false,
    auth: false,
    role: null
},{
    id: 18,
    path: '/reset/:id/:token',
    name: 'Reset Password',
    component: Reset,
    isMenuList: false,
    auth: false,
    role: null
},{
    id: 19,
    path: '/profile/admin/',
    name: 'Profile',
    component: ProfAdmin,
    isMenuList: true,
    auth: true,
    role: roleTypes.ADMIN
}]

export default routes